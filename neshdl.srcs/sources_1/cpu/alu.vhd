library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity alu is
    port (
        a: unsigned(7 downto 0);
        b: unsigned(7 downto 0);
        b_invert: std_logic;
        cin: std_logic;
        srs: in std_logic;
        ands: in std_logic;
        ors: in std_logic;
        eors: in std_logic;

        res: out unsigned(7 downto 0);
        c: out std_logic;
        z: out std_logic;
        n: out std_logic;
        v: out std_logic
    );
end entity alu;

architecture rtl of alu is
    signal add_i: unsigned(8 downto 0);
    signal b_i: unsigned(7 downto 0);
begin
    
    add_i <= ('0' & a) + ('0' & b_i) + (x"00" & cin);
    b_i <= b when b_invert = '0' else not b;

    process(all)
    begin
        if srs = '1' then
            res <= cin & a(7 downto 1);
            c <= a(0);
        else
            if ands = '1' then
                res <= a and b_i;
            elsif ors = '1' then
                res <= a or b_i;
            elsif eors = '1' then
                res <= a xor b_i;
            else
                res <= add_i(7 downto 0);
            end if;
            c <= add_i(8);
        end if;
    end process;

    z <= '1' when res = 0 else '0';
    n <= res(7);
    v <= '1' when a(7) /= res(7) and b_i(7) /= res(7) else '0';
    
end architecture rtl;