library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.common_pkg.all;

entity cpu_r2a03 is
    port (
        clk1_8: in std_logic;   -- 100 MHz system clock
        rst:    in std_logic;   -- System reset
        ce:     in std_logic;   -- Chip enadle

        nmi_n:   in std_logic;   -- Non maskadle interrupt request (active low)
        irq_n:   in std_logic;   -- Interrupt request (active low)
        din:    in unsigned(7 downto 0);    -- Input data
        dout:  out unsigned(7 downto 0);    -- Output data
        addr:  out unsigned(15 downto 0);   -- Output address
        we_n:   out std_logic;               -- write enadle (active low)
        -- Simulation signals
        sim_a:  out unsigned(7 downto 0);
        sim_x:  out unsigned(7 downto 0);
        sim_y:  out unsigned(7 downto 0);
        sim_sp: out unsigned(7 downto 0);
        sim_status: out unsigned(7 downto 0);
        sim_pc: out unsigned(15 downto 0);
        sim_count: out unsigned(31 downto 0);
        sim_write: out std_logic
    );
end cpu_r2a03;

architecture behavioral of cpu_r2a03 is
    -- Clock generation signals
    signal clk_counter: integer range 0 to 64;
    signal clk_tick: std_logic;

    -- Registers signals
    signal q_ac, d_ac: unsigned(7 downto 0);  -- Accumulator register
    signal q_x, d_x:   unsigned(7 downto 0);  -- X register
    signal q_y, d_y:   unsigned(7 downto 0);  -- Y register
    signal q_s, d_s:   unsigned(7 downto 0);  -- Stack pointer register
    signal q_n, d_n:   std_logic;             -- Negative flag
    signal q_v, d_v:   std_logic;             -- Overflow flag
    signal q_b, d_b:   std_logic;             -- Break flag
    signal q_d, d_d:   std_logic;             -- Decimal flag (unused)
    signal q_i, d_i:   std_logic;             -- Interrupt disadle flag
    signal q_z, d_z:   std_logic;             -- Zero flag
    signal q_c, d_c:   std_logic;             -- Carry flag
    signal q_pch, d_pch: unsigned(7 downto 0);  -- PCH register
    signal q_pcl, d_pcl: unsigned(7 downto 0);  -- PCL register

    -- Address bus registers
    signal q_adh, d_adh: unsigned(7 downto 0);  -- adh register
    signal q_adl, d_adl: unsigned(7 downto 0);  -- adl register
    
    -- Data bus registers
    signal q_dl, d_dl:   unsigned(7 downto 0);  -- Data latch register
   
    -- Alu registers
    signal alu_a: unsigned(7 downto 0);    -- ALU a input
    signal alu_b: unsigned(7 downto 0);    -- ALU b input
    signal alu_res: unsigned(7 downto 0);  -- ALU result
    signal alu_c: std_logic;  -- carry out
    signal alu_v: std_logic;  -- overflow out
    signal alu_z: std_logic;  -- zero out
    signal alu_n: std_logic;  -- negative out
    signal q_alu_c: std_logic; -- Carry out latched
    
    -- Instruction registers
    signal q_ir, d_ir: unsigned(7 downto 0);    -- Instruction register
    signal di: cpu_decoded_instruction_t;   -- decoded instruction from q_ir
    
    -- Internal buses
    signal  db: unsigned(7 downto 0);              -- Data bus
    signal  adb: unsigned(15 downto 0);            -- Address bus
    signal pc_plus_1: unsigned(15 downto 0);


    type cpu_state_t is (
        T0, T1, T2_BRANCHK_T3_JSR, T2_PHA, T2_PHP_T4_BRK, T2_PL_T2_RT, T2_JSR, T2_JMPABS_T5_JSR, T2_ABS, T2_BRANCH, T2_IZX_T2_ZPX, T2_IZY_T3_IZX_T3_JMPIND, 
        T2_ZPY, T2_ABX, T2_ABY, READ, WRITE, T3_BRK_T4_JSR, T3_PLA, T3_PLP, T3_RTS_T4_RTI, T3_RTI, T3_BRANCH, T3_IZY, T4_RTS_T5_RTI, T4_JMPIND, T4_IZX,
        T5_BRK, T6_BRK, T5_RTS, PAGE_BOUNDARY, MODIFY, INVALID);
    signal q_t, d_t: cpu_state_t;           -- current & next state
    
    type addr_select_t is (ADDR_PC, ADDR_S, ADDR_AD, ADDR_BRK);
    type alu_select_t is (ALU_DB, ALU_S, ALU_adh, ALU_adl);
    
    -- Control signals
    signal src_a        : std_logic;
    signal src_x        : std_logic;
    signal src_y        : std_logic;
    signal src_s        : std_logic;
    signal src_dl       : std_logic;
    signal src_pcl      : std_logic;
    signal src_pch      : std_logic;
    signal src_p        : std_logic;
    signal dst_a        : std_logic;
    signal dst_x        : std_logic;
    signal dst_y        : std_logic;
    signal dst_dl       : std_logic;
    signal dst_adl      : std_logic;
    signal dst_adh      : std_logic;
    signal dst_pcl      : std_logic;
    signal dst_pch      : std_logic;
    signal dst_s        : std_logic;
    signal load_dl      : std_logic;
    signal load_adl     : std_logic;
    signal load_adh     : std_logic;
    signal load_pcl     : std_logic;
    signal load_pch     : std_logic;
    signal load_nv      : std_logic;
    signal load_czid    : std_logic;
    signal update_c     : std_logic;
    signal update_z     : std_logic;
    signal update_n     : std_logic;
    signal update_v     : std_logic;
    signal clc          : std_logic;
    signal sec          : std_logic;
    signal cli          : std_logic;
    signal sei          : std_logic;
    signal cld          : std_logic;
    signal sed          : std_logic;
    signal inc_pc       : std_logic;
    signal force_carry  : std_logic;
    signal carry_enadle : std_logic;
    signal alu_b_invert : std_logic;
    signal alu_dl       : std_logic;
    signal alu_ands     : std_logic;
    signal alu_eors     : std_logic;
    signal alu_ors      : std_logic;
    signal alu_srs      : std_logic;
    signal write_enable : std_logic;
    signal addr_select  : addr_select_t;
    signal alu_select   : alu_select_t;

    -- Interrupt logic signals
    signal force_break : std_logic;
	signal nmi_reg : std_logic;
	signal nmi_mask : std_logic;	  
	signal nmi_ce : std_logic;
	signal irq_ce : std_logic;
	signal t0_irq_nmi : std_logic;	 
	signal stall_pc : std_logic;
    signal brk_addr: unsigned(14 downto 0);
    signal brk_bit: std_logic;

    -- Simulations signals
    signal sim_status_i: std_logic_vector(7 downto 0);
    signal sim_count_i: unsigned(31 downto 0);
    signal qq_t: cpu_state_t;

begin

    -- Simulation signals
    sim_a <= q_ac;
    sim_x <= q_x;
    sim_y <= q_y;
    sim_pc <= q_pch & q_pcl;
    sim_sp <= q_s;
    sim_count <= sim_count_i;
    sim_write <= '1' when qq_t = T0 and qq_t /= q_t else '0';
    sim_status_i <= q_n & q_v & '1' & q_b & q_d & q_i & q_z & q_c;
    sim_status <= unsigned(sim_status_i);

    dout <= db;
    addr <= adb;
    we_n <= not write_enable;

    -- Latch registers
    latch_tick: process(clk1_8, rst)
    begin
        if rising_edge(clk1_8) then
            if rst = '1' then
                q_pcl   <= x"00";
                q_pch   <= x"80";
                q_dl    <= (others => '0');
                q_ac    <= (others => '0');
                q_x     <= (others => '0');
                q_y     <= (others => '0');
                q_s     <= (others => '0');
                q_adh   <= (others => '0');
                q_adl   <= (others => '0');
                q_ir    <= (others => '0');
                q_alu_c   <= '0';
                q_c     <= '0';
                q_d     <= '0';
                q_i     <= '1';
                q_n     <= '0';
                q_v     <= '0';
                q_b     <= '1';
                q_z     <= '0';
                q_t     <= T0;
                qq_t    <= T0;
                sim_count_i <= to_unsigned(0, sim_count_i'length);
            elsif ce = '1' then
                q_pch   <= d_pch;
                q_pcl   <= d_pcl;
                q_dl    <= d_dl;
                q_ac    <= d_ac;
                q_x     <= d_x;
                q_y     <= d_y;
                q_s     <= d_s;
                q_adh   <= d_adh;
                q_adl   <= d_adl;
                q_alu_c <= alu_c;
                q_ir    <= d_ir;
                q_c     <= d_c;
                q_d     <= d_d;
                q_i     <= d_i;
                q_n     <= d_n;
                q_v     <= d_v;
                q_b     <= d_b;
                q_z     <= d_z;
                q_t     <= d_t;
                qq_t    <= q_t;
                sim_count_i <= sim_count_i + 1;
            end if;
        end if;
    end process latch_tick;


    -- Instruction decoder
    instr_decoder_c: entity work.decoder
        port map (
            ir => q_ir,
            di => di
        );

    -- Timing generation
    timing_gen: process(all)
    begin
        d_t <= T0;
        case q_t is
            when T0 =>
                d_t <= T1;
            when T1 =>
                if di.brk = '1' then
                    d_t <= T2_BRANCHK_T3_JSR;
                elsif di.pha = '1' then
                    d_t <= T2_PHA;
                elsif di.php = '1' then
                    d_t <= T2_PHP_T4_BRK;
                elsif di.plp = '1' or di.pla = '1' or di.rts = '1' or di.rti = '1' then
                    d_t <= T2_PL_T2_RT;
                elsif di.jsr = '1' then
                    d_t <= T2_JSR;
                elsif di.jmpabs = '1' then
                    d_t <= T2_JMPABS_T5_JSR;
                elsif di.ab = '1' then
                    d_t <= T2_ABS;
                elsif (di.bcc = '1' and q_c = '0') or
                      (di.bcs = '1' and q_c = '1') or
                      (di.beq = '1' and q_z = '1') or
                      (di.bmi = '1' and q_n = '1') or
                      (di.bne = '1' and q_z = '0') or
                      (di.bpl = '1' and q_n = '0') or
                      (di.bvc = '1' and q_v = '0') or
                      (di.bvs = '1' and q_v = '1') then
                    d_t <= T2_BRANCH;
                elsif di.zp_index = '1' and di.index_y = '1' then
                    d_t <= T2_ZPY;
                elsif di.zp_index = '1' or di.indirect_x = '1' then
                    d_t <= T2_IZX_T2_ZPX;
                elsif di.ab_index = '1' and di.index_y = '1' then
                    d_t <= T2_ABY;
                elsif di.ab_index = '1' then
                    d_t <= T2_ABX;
                elsif di.indirect_y = '1' then
                    d_t <= T2_IZY_T3_IZX_T3_JMPIND;
                elsif di.zp = '1' and (di.sta = '1' or di.stx = '1' or di.sty = '1') then
                    d_t <= WRITE;
                elsif di.zp = '1' then
                    d_t <= READ;
                else
                    d_t <= T0;
                end if;
            when T2_BRANCHK_T3_JSR =>
                d_t <= T3_BRK_T4_JSR;
            when T2_PHP_T4_BRK =>
                if di.php = '1' then
                    d_t <= T0;
                elsif di.brk = '1' then
                    d_t <= T5_BRK;
                else
                    d_t <= INVALID;
                end if;
            when T2_PL_T2_RT =>
                if di.pla = '1' then
                    d_t <= T3_PLA;
                elsif di.plp = '1' then
                    d_t <= T3_PLP;
                elsif di.rti = '1' then
                    d_t <= T3_RTI;
                else
                    d_t <= T3_RTS_T4_RTI;
                end if;
            when T2_JSR =>
                d_t <= T2_BRANCHK_T3_JSR;
            when T2_ABS | T2_IZX_T2_ZPX =>
                if di.jmpind = '1' or di.indirect_x = '1' then
                    d_t <= T2_IZY_T3_IZX_T3_JMPIND;
                elsif di.sta = '1' or di.stx = '1' or di.sty = '1' then
                    d_t <= WRITE;
                else
                    d_t <= READ;
                end if;
            when T2_BRANCH =>
                if q_dl(7) = alu_c then
                    d_t <= T0;
                else
                    d_t <= T3_BRANCH;
                end if;
            when T2_IZY_T3_IZX_T3_JMPIND =>
                if di.indirect_x = '1' then
                    d_t <= T4_IZX;
                elsif di.indirect_y = '1' then
                    d_t <= T3_IZY;
                elsif di.jmpind = '1' then
                    d_t <= T4_JMPIND;
                else
                    d_t <= INVALID;
                end if;
            when T2_ZPY | T4_IZX =>
                if di.sta = '1' or di.stx = '1' or di.sty = '1' then
                    d_t <= WRITE;
                else
                    d_t <= READ;
                end if;
            when T2_ABX | T2_ABY | T3_IZY =>
                d_t <= PAGE_BOUNDARY;
            when READ =>
                if di.read_mod_write = '1' then
                    d_t <= MODIFY;
                else
                    d_t <= T0;
                end if;
            when T3_BRK_T4_JSR =>
                if di.brk = '1' then
                    d_t <= T2_PHP_T4_BRK;
                elsif di.jsr = '1' then
                    d_t <= T2_JMPABS_T5_JSR;
                else
                    d_t <= INVALID;
                end if;
            when T3_RTS_T4_RTI =>
                d_t <= T4_RTS_T5_RTI;
            when T3_RTI =>
                d_t <= T3_RTS_T4_RTI;
            when T4_RTS_T5_RTI =>
                if di.rts = '1' then
                    d_t <= T5_RTS;
                else
                    d_t <= T0;
                end if;
            when T5_BRK =>
                d_t <= T6_BRK;
            when PAGE_BOUNDARY =>
                if di.sta = '1' or di.stx = '1' or di.sty = '1' then
                    d_t <= WRITE;
                elsif di.read_mod_write = '1' or q_alu_c = '1' then
                    d_t <= READ;
                else
                    d_t <= T0;
                end if;
            when MODIFY =>
                d_t <= WRITE;
            when INVALID => assert false report "Invalid state" severity error;
            when others =>
                d_t <= T0;  
        end case;
    end process timing_gen;


    -- Interrupt logic
    nmi_ce <= not (force_break or nmi_n or nmi_mask);
    irq_ce <= not (force_break or irq_n or q_i);
    t0_irq_nmi <= (nmi_ce or irq_ce) when q_t = T0 else '0';
    stall_pc <= t0_irq_nmi or not q_b;

    irq_reg: process(clk1_8, rst)
    begin
        if rst = '1' then
            force_break <= '1';
            nmi_reg <= '0';
            nmi_mask <= '0';
        elsif rising_edge(clk1_8) then
            if ce = '1' and (q_t = T0 or q_t = T6_BRK) then
                nmi_reg <= nmi_ce;
                nmi_mask <= nmi_ce or (nmi_mask and not nmi_n);
                if q_t = T6_BRK then
                    force_break <= '0';
                end if;
            end if;
        end if;
    end process irq_reg;


    brk_addr <= x"FFF" & '1' & not nmi_reg & not force_break;
    -- Control signals generator
    control_signals_gen: process(all)
    begin
        src_a        <= '0';
        src_x        <= '0';
        src_y        <= '0';
        src_s        <= '0';
        src_dl       <= '0';
        src_pcl      <= '0';
        src_pch      <= '0';
        src_p        <= '0';
        dst_a        <= '0';
        dst_x        <= '0';
        dst_y        <= '0';
        dst_dl       <= '0';
        dst_adl      <= '0';
        dst_adh      <= '0';
        dst_pcl      <= '0';
        dst_pch      <= '0';
        dst_s        <= '0';
        load_dl      <= '0';
        load_adl     <= '0';
        load_adh     <= '0';
        load_pcl     <= '0';
        load_pch     <= '0';
        load_nv      <= '0';
        load_czid    <= '0';
        update_c     <= '0';
        update_z     <= '0';
        update_n     <= '0';
        update_v     <= '0';
        clc          <= '0';
        sec          <= '0';
        cli          <= '0';
        sei          <= '0';
        cld          <= '0';
        sed          <= '0';
        inc_pc       <= '0';
        force_carry  <= '0';
        carry_enadle <= '0';
        alu_b_invert <= '0';
        alu_dl       <= '0';
        alu_ands     <= '0';
        alu_eors     <= '0';
        alu_ors      <= '0';
        alu_srs      <= '0';
        write_enable <= '0';
        addr_select  <= ADDR_PC;
        alu_select   <= ALU_DB;
        brk_bit      <= '0';

        case q_t is
            when T0 => -- Execute and fetch the next instruction
                inc_pc <= '1';

                -- Source
			    src_a <= di.tay or di.tax or di.cmp or di.accumulator or di.ora
                            or di.aand or di.eor or di.bitt or di.adc or di.sbc;
                src_x <= di.inx or di.dex or di.txa or di.txs or di.cpx;
                src_y <= di.iny or di.dey or di.tya or di.cpy;
                src_dl <= di.pla or di.lda or di.ldy or (di.ldx and not di.tsx); 
                src_s <= di.tsx;
                
                -- Destination
                dst_a <= di.pla or di.accumulator or di.txa or di.tya or di.ora
                            or di.aand or di.eor or di.adc or di.sbc or di.lda;
                dst_x <= di.inx or di.dex or di.tax or di.tsx or di.ldx;
                dst_y <= di.iny or di.dey or di.tay or di.ldy;
                dst_s <= di.txs;
                
                -- Function
                alu_ors <= di.ora;
                alu_eors <= di.eor;
                alu_ands <= di.aand or di.bitt;
                alu_srs <= di.lsr or di.rotr;
                force_carry <= di.iny or di.inx or di.cmp or di.cpx or di.cpy;
                carry_enadle <= di.adc or di.sbc or di.rotl or di.rotr;
                alu_b_invert <= di.dey or di.dex or di.sbc or di.cmp or di.cpx or di.cpy;
                alu_dl <= di.ora or di.aand or di.eor or di.bitt or di.adc or
                             di.sbc or di.cmp or di.cpx or di.cpy or di.asl or di.rotl;
                
                -- Update Flags   
                update_n <= di.pla or di.accumulator or di.txa or di.tya or di.ora
                                or di.aand or di.eor or di.adc or di.sbc or di.lda or di.inx or
                                di.dex or di.tax or di.cpx or di.tsx or di.iny or di.ldx or
                                di.dey or di.tay or di.ldy or di.cmp or di.cpy;
                update_z <= di.pla or di.accumulator or di.txa or di.tya or di.ora
                                or di.aand or di.eor or di.adc or di.sbc or di.lda or di.inx or
                                di.dex or di.tax or di.cpx or di.tsx or di.iny or di.ldx or
                                di.dey or di.tay or di.ldy or di.cmp or di.cpy or di.bitt;   
                update_c <= di.adc or di.sbc or di.cmp or di.cpx or di.cpy or di.accumulator;
                update_v <= di.adc or di.sbc or di.clv;
                
                -- Clear or set flags
                clc <= di.clc;
                sec <= di.sec;
                cli <= di.cli;
                sei <= di.sei;
                cld <= di.cld;
                sed <= di.sed;
            when T1 => -- Single Byte: DL = A   
                if di.single_byte = '1' then
                    dst_dl <= '1';
                    src_a <= '1';
                else  -- Multi Byte: DL,ADL = [PC++], ADH = 0
                    load_dl <= '1';
                    load_adl <= '1';
                    dst_adh <= '1';
                    force_carry <= '1'; -- ALU output = 0 (0xFF + 1)
                    inc_pc <= '1';	
                end if;			
												
			when T2_BRANCHK_T3_JSR => -- [S--] = PCH
				write_enable <= not force_break;
				src_pch <= '1';
				alu_b_invert <= '1'; 
				dst_s <= '1';
                addr_select <= ADDR_S;
                alu_select <= ALU_S;
			when T2_PHA => -- [S--] = A
				write_enable <= '1';
				src_a <= '1';
				alu_b_invert <= '1';
				dst_s <= '1';
                addr_select <= ADDR_S;
                alu_select <= ALU_S;
			when T2_PHP_T4_BRK => -- [S--] = P
				write_enable <= not force_break;
				src_p <= '1';
				alu_b_invert <= '1';
				dst_s <= '1';
                addr_select <= ADDR_S;
                alu_select <= ALU_S;
			when T2_PL_T2_RT => -- [S++]
				force_carry <= '1';
				dst_s <= '1';
                addr_select <= ADDR_S;
                alu_select <= ALU_S;
			when T2_JSR => -- [S]
                addr_select <= ADDR_S;
			when T2_JMPABS_T5_JSR => -- PCH=[PC]; PCL=ADL
				load_pch <= '1';				
				dst_pcl <= '1';
                alu_select <= ALU_adl;
			when T2_ABS => -- ADH = [PC++]
				load_adh <= '1';
				inc_pc <= '1';
			when T2_BRANCH => -- PCL=PCL+DL; [PC]
				src_pcl <= '1';
				alu_dl <= '1';
				dst_pcl <= '1';
			when T2_IZX_T2_ZPX => -- ADL=X+DL; [AD]
				src_x <= '1';
				alu_dl <= '1';
				dst_adl <= '1';
                addr_select <= ADDR_AD;		
			when T2_IZY_T3_IZX_T3_JMPIND => -- DL=[ADL++]
				load_dl <= '1';
				dst_adl <= '1';
				force_carry <= '1';
                addr_select <= ADDR_AD;
                alu_select <= ALU_adl;
			when T2_ZPY => -- ADL=Y+DL; [AD]
				src_y <= '1';
				alu_dl <= '1';
				dst_adl <= '1';
                addr_select <= ADDR_AD;
			when T2_ABX => -- ADH=[PC++]; ADL=X+DL
				src_x <= '1';
				dst_adl <= '1';
				alu_dl <= '1';
				load_adh <= '1';
				inc_pc <= '1';
			when T2_ABY => -- ADH=[PC++]; ADL=Y+DL
				src_y <= '1';
				dst_adl <= '1';
				alu_dl <= '1';
				load_adh <= '1';
				inc_pc <= '1';
			when READ => -- DL = [AD]
				load_dl <= '1';
				load_nv <= di.bitt;
                addr_select <= ADDR_AD;
			when WRITE => -- [AD] = reg
				src_a <= di.sta;
				src_x <= di.stx;
				src_y <= di.sty;
				src_dl <= di.read_mod_write;
				write_enable <= '1';
                addr_select <= ADDR_AD;
			when T3_BRK_T4_JSR => -- [S--] = PCL
                write_enable <= not force_break;
				alu_b_invert <= '1';
				dst_s <= '1';
				src_pcl <= '1';
                addr_select <= ADDR_S;
                alu_select <= ALU_S;
			when T3_PLA => -- DL=[S]
				load_dl <= '1';
                addr_select <= ADDR_S;
			when T3_PLP => -- P=[S]
				load_czid <= '1';
				load_nv <= '1';
                addr_select <= ADDR_S;
			when T3_RTS_T4_RTI => -- PCL=[S++]
				force_carry <= '1';
				dst_s <= '1';
				load_pcl <= '1';
                addr_select <= ADDR_S;
                alu_select <= ALU_S;
			when T3_RTI => -- P=[S++]
				force_carry <= '1';
				dst_s <= '1';
				load_czid <= '1';
				load_nv <= '1';
                addr_select <= ADDR_S;
                alu_select <= ALU_S;
			when T3_BRANCH =>	-- if (carry_reg) PCH++; else PCH--;
				force_carry <= q_alu_c;
                alu_b_invert <= not q_alu_c;		
				src_pch <= '1';
				dst_pch <= '1';
			when T3_IZY => -- ADH=[AD]; ADL=DL+Y
				src_y <= '1';
				dst_adl <= '1';
				alu_dl <= '1';
				load_adh <= '1';
                addr_select <= ADDR_AD;
			when T4_RTS_T5_RTI => -- PCH=[S]
				load_pch <= '1';
                addr_select <= ADDR_S;
			when T4_JMPIND => -- PCH=[AD]; PCL=DL
				load_pch <= '1';
				src_dl <= '1';
				dst_pcl <= '1';
                addr_select <= ADDR_AD;
			when T4_IZX => -- ADH = [AD]; ADL=DL
				load_adh <= '1';
				src_dl <= '1';
				dst_adl <= '1';
                addr_select <= ADDR_AD;
			when T5_BRK => -- PCL = [VECTOR_LOW]
				load_pcl <= '1';
                addr_select <= ADDR_BRK;
                brk_bit <= '0';
			when T6_BRK => -- PCH = [VECTOR_HIGH]
				load_pch <= '1';
                addr_select <= ADDR_BRK;
                brk_bit <= '1';
			when T5_RTS => -- [PC++]
				inc_pc <= '1';
			when PAGE_BOUNDARY => -- DL = [AD], page boundry crossed: ADH++
				dst_adh <= q_alu_c;				
				force_carry <= '1';
				load_dl <= '1';
                addr_select <= ADDR_AD;
                alu_select <= ALU_adh;
			when MODIFY =>	
				write_enable <= '1';
				src_dl <= '1';
				dst_dl <= '1';
				alu_srs <= di.rotr or di.lsr;
				alu_dl <= di.rotl or di.asl;
				carry_enadle <= di.rotr or di.rotl;
				force_carry <= di.inc;
				alu_b_invert <= di.dec;
				update_z <= '1';
				update_n <= '1';
				update_c <= di.rotl or di.asl or di.rotr or di.lsr;
				addr_select <= ADDR_AD; 
			when INVALID =>
			 assert false report "Invalid state!" severity error;
        end case;
    end process control_signals_gen;


    -- Registers next values
    d_ir <= x"00" when force_break = '1' or t0_irq_nmi = '1' else
            din   when q_t = T0 else
            q_ir;

    d_dl <= din     when load_dl = '1' else
            alu_res when dst_dl = '1' else
            q_dl;

    d_adl <= din     when load_adl = '1' else
             alu_res when dst_adl = '1' else
             q_adl;
    
    d_adh <= din     when load_adh = '1' else
             alu_res when dst_adh = '1' else
             q_adh;

    d_ac <= alu_res when dst_a = '1' else
            q_ac;
    
    d_x <= alu_res when dst_x = '1' else
           q_x;
    
    d_y <= alu_res when dst_y = '1' else
           q_y;

    d_s <= alu_res when dst_s = '1' else
           q_s;
    
    pc_plus_1 <= (q_pch & q_pcl) + 1;
    d_pcl <= pc_plus_1( 7 downto 0) when (inc_pc = '1' and stall_pc = '0') else
             din                    when load_pcl = '1' else
             alu_res                when dst_pcl = '1' else 
             q_pcl;
    d_pch <= pc_plus_1(15 downto 8) when (inc_pc = '1' and stall_pc = '0') else
             din                    when load_pch = '1' else
             alu_res                when dst_pch = '1' else
             q_pch;
    
    d_c <= din(0) when load_czid = '1' else
           alu_c  when update_c = '1' else
           '0'    when clc = '1' else
           '1'    when sec = '1' else
           q_c;
    
    d_z <= din(1) when load_czid = '1' else
           alu_z  when update_z = '1' else
           q_z;
    
    d_i <= din(2) when load_czid = '1' else
           '0'    when cli = '1' else
           '1'    when sei = '1' else
           q_i;

    d_b <= not t0_irq_nmi when (q_t = T0 or q_t = T6_BRK) else
           q_b;

    d_d <= din(3) when load_czid = '1' else
           '0'    when cld = '1' else
           '1'    when sed = '1' else
           q_d;

    d_v <= din(6) when load_nv = '1' else
           alu_v  when update_v = '1' else
           q_v;

    d_n <= din(7) when load_nv = '1' else
           alu_n  when update_n = '1' else
           q_n;

    alu_b <= q_dl when alu_dl = '1' else
             x"00";
    
    with alu_select select
    alu_a <= q_adh when ALU_adh,
             q_adl when ALU_adl,
             db    when ALU_DB,
             q_s   when ALU_S;
    
    alu: entity work.alu
        port map (
            a => alu_a,
            b => alu_b,
            b_invert => alu_b_invert,
            cin => force_carry or (carry_enadle and q_c),
            srs => alu_srs,
            ands => alu_ands,
            ors => alu_ors,
            eors => alu_eors,
            res => alu_res,
            c => alu_c,
            z => alu_z,
            n => alu_n,
            v => alu_v
        );

    db <= q_ac      when src_a = '1' else
          q_x       when src_x = '1' else
          q_y       when src_y = '1' else
          q_s       when src_s = '1' else
          q_dl      when src_dl = '1' else
          q_pcl     when src_pcl = '1' else
          q_pch     when src_pch = '1' else
          (q_n & q_v & '1' & q_b & q_d & q_i & q_z & q_c) when src_p = '1' else
          x"FF";
    
    with addr_select select
    adb <= q_pch & q_pcl when ADDR_PC,
           q_adh & q_adl when ADDR_AD,
           X"01" & q_s   when ADDR_S,
           brk_addr & brk_bit when ADDR_BRK;
    
end architecture behavioral;