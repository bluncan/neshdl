library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

use work.common_pkg.all;

entity decoder is
    port (
        ir: in unsigned(7 downto 0);
        di: out cpu_decoded_instruction_t
    );
end entity decoder;

architecture rtl of decoder is
    signal aaa: unsigned(2 downto 0);
    signal bbb: unsigned(2 downto 0);
    signal cc: unsigned(1 downto 0);
begin
    
    aaa <= ir(7 downto 5);
    bbb <= ir(4 downto 2);
    cc  <= ir(1 downto 0);

    process(all)
    begin
        di <= (others => '0');
        case ir is
            when x"69" | x"65" | x"75" | x"6D" | x"7D" | x"79" | x"61" | x"71" => di.adc <= '1';
            when x"29" | x"25" | x"35" | x"2D" | x"3D" | x"39" | x"21" | x"31" => di.aand <= '1';
            when x"0A" => di.asl <= '1'; di.accumulator <= '1'; di.single_byte <= '1';
            when x"06" | x"16" | x"0E" | x"1E" => di.asl <= '1'; di.read_mod_write <= '1';
            when x"90" => di.bcc <= '1';
            when x"B0" => di.bcs <= '1';
            when x"F0" => di.beq <= '1';
            when x"30" => di.bmi <= '1';
            when x"D0" => di.bne <= '1';
            when x"10" => di.bpl <= '1';
            when x"50" => di.bvc <= '1';
            when x"70" => di.bvs <= '1';
            when x"24" | x"2C" => di.bitt <= '1';
            when x"00" => di.brk <= '1';
            when x"18" => di.clc <= '1'; di.single_byte <= '1';
            when x"38" => di.sec <= '1'; di.single_byte <= '1';
            when x"D8" => di.cld <= '1'; di.single_byte <= '1';
            when x"F8" => di.sed <= '1'; di.single_byte <= '1';
            when x"58" => di.cli <= '1'; di.single_byte <= '1';
            when x"78" => di.sei <= '1'; di.single_byte <= '1';
            when x"B8" => di.clv <= '1'; di.single_byte <= '1';
            when x"C9" | x"C5" | x"D5" | x"CD" | x"DD" | x"D9" | x"C1" | x"D1" => di.cmp <= '1';
            when x"E0" | x"E4" | x"EC" => di.cpx <= '1';
            when x"C0" | x"C4" | x"CC" => di.cpy <= '1';
            when x"C6" | x"D6" | x"CE" | x"DE" => di.dec <= '1'; di.read_mod_write <= '1';
            when x"CA" => di.dex <= '1'; di.single_byte <= '1';
            when x"88" => di.dey <= '1'; di.single_byte <= '1';
            when x"49" | x"45" | x"55" | x"4D" | x"5D" | x"59" | x"41" | x"51" => di.eor <= '1';
            when x"E6" | x"F6" | x"EE" | x"FE" => di.inc <= '1'; di.read_mod_write <= '1';
            when x"E8" => di.inx <= '1'; di.single_byte <= '1';
            when x"C8" => di.iny <= '1'; di.single_byte <= '1';
            when x"4C" => di.jmpabs <= '1';
            when x"6C" => di.jmpind <= '1';
            when x"20" => di.jsr <= '1';
            when x"A9" | x"A5" | x"B5" | x"AD" | x"BD" | x"B9" | x"A1" | x"B1" => di.lda <= '1';
            when x"A2" | x"A6" | x"B6" | x"AE" | x"BE" => di.ldx <= '1';
            when x"A0" | x"A4" | x"B4" | x"AC" | x"BC" => di.ldy <= '1';
            when x"4A" => di.lsr <= '1'; di.accumulator <= '1'; di.single_byte <= '1';
            when x"46" | x"56" | x"4E" | x"5E" => di.lsr <= '1'; di.read_mod_write <= '1';
            when x"EA" => di.single_byte <= '1'; -- NOP 
            when x"09" | x"05" | x"15" | x"0D" | x"1D" | x"19" | x"01" | x"11" => di.ora <= '1';
            when x"48" => di.pha <= '1'; di.single_byte <= '1';
            when x"08" => di.php <= '1'; di.single_byte <= '1';
            when x"68" => di.pla <= '1'; di.single_byte <= '1';
            when x"28" => di.plp <= '1'; di.single_byte <= '1';
            when x"2A" => di.rotl <= '1'; di.accumulator <= '1'; di.single_byte <= '1';
            when x"26" | x"36" | x"2E" | x"3E" => di.rotl <= '1'; di.read_mod_write <= '1';
            when x"6A" => di.rotr <= '1'; di.accumulator <= '1'; di.single_byte <= '1';
            when x"66" | x"76" | x"6E" | x"7E" => di.rotr <= '1'; di.read_mod_write <= '1';
            when x"40" => di.rti <= '1';
            when x"60" => di.rts <= '1';
            when x"E9" | x"E5" | x"F5" | x"ED" | x"FD" | x"F9" | x"E1" | x"F1" => di.sbc <= '1';
            when x"85" | x"95" | x"8D" | x"9D" | x"99" | x"81" | x"91" => di.sta <= '1';
            when X"86" | x"96" | x"8E" => di.stx <= '1';
            when x"84" | x"94" | x"8C" => di.sty <= '1';
            when x"AA" => di.tax <= '1'; di.single_byte <= '1';
            when x"A8" => di.tay <= '1'; di.single_byte <= '1';
            when x"BA" => di.tsx <= '1'; di.single_byte <= '1';
            when x"8A" => di.txa <= '1'; di.single_byte <= '1';
            when x"9A" => di.txs <= '1'; di.single_byte <= '1';
            when x"98" => di.tya <= '1'; di.single_byte <= '1';
            when others => null;
        end case;

        if bbb = 1 then
            di.zp <= '1';
        elsif bbb = 3 then
            di.ab <= '1';
        elsif bbb = 5 then
            di.zp_index <= '1';
        elsif (bbb = 6 and cc = 1) or bbb = 7 then
            di.ab_index <= '1';
        elsif bbb = 0 and cc = 1 then
            di.indirect_x <= '1';
        elsif bbb = 4 and cc = 1 then
            di.indirect_y <= '1';
        end if;

        if (bbb = 4 and cc = 1) or
           (bbb = 6 and cc = 1) or
           ((aaa = 4 or aaa = 5) and (bbb = 5 or bbb = 7) and cc = 2)
        then
            di.index_y <= '1';
        end if; 
    end process;
    
end architecture rtl;