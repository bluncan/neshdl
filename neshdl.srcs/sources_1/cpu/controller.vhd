library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

use work.common_pkg.all;

entity controller is
    port (
        clk100: in std_logic;
        rst: in std_logic;
        we: in std_logic;
        addr: in unsigned(15 downto 0);
        din:  in std_logic;
        controller1_data: in std_logic;
        controller2_data: in std_logic;

        clk: out std_logic;
        latch: out std_logic;
        dout: out unsigned(7 downto 0)
    );
end entity controller;

architecture rtl of controller is
    signal q_reg1, d_reg1: unsigned(7 downto 0);
    signal q_reg2, d_reg2: unsigned(7 downto 0);
    signal q_addr: unsigned(15 downto 0);
    signal q_controller1_read, d_controller1_read: unsigned(8 downto 0);
    signal q_controller2_read, d_controller2_read: unsigned(8 downto 0);
    signal q_start, d_start: std_logic;

    signal clk_counter: unsigned(14 downto 0);
    signal q_cycle: unsigned(3 downto 0);
    signal q_data1: unsigned(7 downto 0);
    signal q_data2: unsigned(7 downto 0);

    signal reread: std_logic;
    signal clk_s: std_logic;

begin

    latch <= '1' when q_cycle = 0 else '0';
    clk <= '1' when (clk_s = '1' and q_cycle > 0 and q_cycle < 9) else '0';
    
    process(clk100)
    begin
        if rising_edge(clk100) then
            if rst = '1' then
                q_addr <= (others => '0');
                q_controller1_read <= (others => '0');
                q_controller2_read <= (others => '0');
                q_start <= '0';
                clk_counter <= (others => '0');
            else
                q_addr <= addr;
                q_controller1_read <= d_controller1_read;
                q_controller2_read <= d_controller2_read;
                q_start <= d_start;
                clk_counter <= clk_counter + 1;
            end if;
        end if;
    end process;

    clk_s <= clk_counter(14);
    reread <= '0' when addr /= q_addr else '1';

    process(all)
    begin
        if rising_edge(clk_s) then
            q_cycle <= q_cycle + 1;
            if q_cycle = 0 then
                q_data1 <= (others => '0');
                q_data2 <= (others => '0');
            elsif q_cycle > 0 and q_cycle < 9 then
                q_data1 <= (not controller1_data) & q_data1(7 downto 1);
                q_data2 <= (not controller2_data) & q_data2(7 downto 1);
            elsif q_cycle = 9 then
                q_reg1 <= q_data1;
                q_reg2 <= q_data2;
                q_cycle <= (others => '0');
            end if;
        end if;
    end process;

    
    process(all)
    begin
        dout <= (others => '0');
        d_controller1_read <= q_controller1_read;
        d_controller2_read <= q_controller2_read;
        d_start <= q_start;

        if addr = x"4016" or addr = x"4017" then
            if addr = x"4016" then
                dout <= "0000000" & q_controller1_read(0);
            else
                dout <= "0000000" & q_controller2_read(0);
            end if;

            if reread = '0' then
                if we = '1' and addr = x"4016" then
                    if q_start = '0' and din = '1' then
                        d_start <= '1';
                    elsif q_start = '1' and din = '0' then
                        d_start <= '0';
                        d_controller1_read <= q_reg1 & '0';
                        d_controller2_read <= q_reg2 & '0';
                    end if;
                elsif we = '0' and addr = x"4016" then
                    d_controller1_read <= '1' & q_controller1_read(8 downto 1);
                elsif we = '0' and addr = x"4017" then
                    d_controller2_read <= '1' & q_controller2_read(8 downto 1);
                end if;
            end if;
        end if;
    end process;

end architecture rtl;