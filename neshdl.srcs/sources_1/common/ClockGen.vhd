library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity clock_gen is
    port (
        clk100:   in std_logic;
        rst:      in std_logic;
        clk_cpu: out std_logic;
        clk_ppu: out std_logic;
        clk_vga: out std_logic
    );
end entity clock_gen;

architecture rtl of clock_gen is
    signal clk_counter_cpu: integer range 0 to 64;
    signal clk_counter_ppu: integer range 0 to 32;
    signal clk_counter_vga: unsigned(1 downto 0);

    signal clk_cpu_i, clk_ppu_i: std_logic;
begin
    
    gen_cpu_clk: process(clk100)
    begin
        if rising_edge(clk100) then
            if rst = '1' then
                clk_counter_cpu <= 0;
                clk_cpu_i <= '0';
            else
                clk_counter_cpu <= clk_counter_cpu + 1;
                if clk_counter_cpu = 28 then -- 55
                    clk_counter_cpu <= 0;
                    clk_cpu_i <= not clk_cpu_i;
                end if;
            end if;
        end if;
    end process gen_cpu_clk;


    gen_ppu_clk: process(clk100)
    begin
        if rising_edge(clk100) then
            if rst = '1' then
                clk_counter_ppu <= 0;
                clk_ppu_i <= '0';
            else
                clk_counter_ppu <= clk_counter_ppu + 1;
                if clk_counter_ppu = 9 then -- 18
                    clk_counter_ppu <= 0;
                    clk_ppu_i <= not clk_ppu_i;
                end if;
            end if;
        end if;
    end process gen_ppu_clk;


    gen_vga_clk: process(clk100)
    begin
        if rising_edge(clk100) then
            if rst = '1' then
                clk_counter_vga <= (others => '0');
            else
                clk_counter_vga <= clk_counter_vga + 1;
            end if;
        end if; 
    end process gen_vga_clk;
    
    
    clk_vga <= clk_counter_vga(1);
    clk_cpu <= clk_cpu_i;
    clk_ppu <= clk_ppu_i;
    
end architecture rtl;