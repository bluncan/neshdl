library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity vga_core is
    port (
        clk25:  in std_logic;
        rst:     in std_logic;

        hsync:   out std_logic;
        vsync:   out std_logic;
        x:       out unsigned(9 downto 0);
        y:       out unsigned(9 downto 0)
    );
end entity vga_core;

architecture rtl of vga_core is

    signal H_DISP: integer:= 640;
    signal H_FP  : integer:= 16;
    signal H_RT  : integer:= 96;
    signal H_BP  : integer:= 48;

    signal V_DISP: integer:= 480;
    signal V_FP  : integer:= 10;
    signal V_RT  : integer:= 2;
    signal V_BP  : integer:= 29;

    signal q_hsync, d_hsync: std_logic;
    signal q_vsnyc, d_vsync: std_logic;
    signal q_hcounter, d_hcounter: unsigned(9 downto 0);
    signal q_vcounter, d_vcounter: unsigned(9 downto 0);
    signal increment_v: std_logic;
begin

    process(clk25)
    begin
        if rising_edge(clk25) then
            if rst = '1' then
                q_hcounter <= (others => '0');
                increment_v <= '0';
            else
                q_hcounter <= q_hcounter + 1;
                increment_v <= '0';
                if q_hcounter = H_DISP + H_FP + H_RT + H_BP - 1 then
                    q_hcounter <= (others => '0');
                    increment_v <= '1';
                end if;
            end if;
        end if;
    end process;

    process(clk25)
    begin
        if rising_edge(clk25) then
            if rst = '1' then
                q_vcounter <= (others => '0');
            elsif increment_v = '1' then
                q_vcounter <= q_vcounter + 1;
                if q_vcounter = V_DISP + V_FP + V_RT + V_BP - 1 then
                    q_vcounter <= (others => '0');
                end if;
            end if;
        end if;
    end process;

    process(clk25)
    begin
        if rising_edge(clk25) then
            if rst = '1' then
                q_vsnyc <= '0';
                q_hsync <= '0';
            else
                if q_hcounter >= (H_DISP + H_FP) and q_hcounter < (H_DISP + H_FP + H_RT) then
                    q_hsync <= '1';
                else
                    q_hsync <= '0';
                end if;
                
                if q_vcounter >= (V_DISP + V_FP) and q_vcounter < (V_DISP + V_FP + V_RT) then
                    q_vsnyc <= '1';
                else
                    q_vsnyc <= '0';
                end if;
           end if;
        end if;
    end process;

    hsync <= q_hsync;
    vsync <= q_vsnyc;
    x <= q_hcounter;
    y <= q_vcounter;

end architecture rtl;