library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity single_port_ram is
    generic (
        address_width: integer;
        data_width: integer := 8
    );
    port (
        clk100: in std_logic;
        ce:     in std_logic;
        we:     in std_logic;
        addr:   in unsigned(address_width-1 downto 0);
        din:    in unsigned(data_width-1 downto 0);
        dout:   out unsigned(data_width-1 downto 0)
    );
end entity single_port_ram;

architecture behavioral of single_port_ram is
    type rom_t is array(0 to 2**address_width-1) of unsigned(data_width-1 downto 0);
    signal ram: rom_t;
    signal q_addr: unsigned(address_width-1 downto 0);
begin

    process(clk100)
    begin
        if rising_edge(clk100) then
            if ce = '1' then
                if we = '1' then
                    ram(to_integer(addr)) <= din;
                end if;
                q_addr <= addr;
            end if;
        end if;
    end process;

    dout <= ram(to_integer(q_addr));

end architecture;