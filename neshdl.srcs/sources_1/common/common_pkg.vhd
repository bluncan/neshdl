library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

package common_pkg is

    type cpu_decoded_instruction_t is record
        aand, ab, ab_index, accumulator, adc, asl, bcc, bcs, beq,
        bitt, bmi, bne, bpl, brk, bvc, bvs, clc, cld, cli, clv, cmp,
        cpx, cpy, dec, dex, dey, eor, inc, index_y, indirect_x, indirect_y,
        inx, iny, jmpabs, jmpind, jsr, lda, ldx, ldy, lsr, ora, pha, php, pla,
        plp, read_mod_write, rotl, rotr, rti, rts, sbc, sec, sed, sei, single_byte,
        sta, stx, sty, tax, tay, tsx, txa, txs, tya, zp, zp_index : std_logic;
    end record;

    type cpu_state_t is (
        T0, T1, T2_BRANCHK_T3_JSR, T2_PHA, T2_PHP_T4_BRK, T2_PL_T2_RT, T2_JSR, T2_JMPABS_T5_JSR, T2_ABS, T2_BRANCH, T2_IZX_T2_ZPX, T2_IZY_T3_IZX_T3_JMPIND, 
        T2_ZPY, T2_ABX, T2_ABY, READ, WRITE, T3_BRK_T4_JSR, T3_PLA, T3_PLP, T3_RTS_T4_RTI, T3_RTI, T3_BRANCH, T3_IZY, T4_RTS_T5_RTI, T4_JMPIND, T4_IZX,
        T5_BRK, T6_BRK, T5_RTS, PAGE_BOUNDARY, MODIFY, INVALID
    );

    type addr_select_t is (ADDR_PC, ADDR_S, ADDR_AD, ADDR_BRK);
    type alu_select_t is (ALU_DB, ALU_S, ALU_adh, ALU_adl);
    type src_select_t is (SRC_A, SRC_X, SRC_Y, SRC_DL, SRC_S, SRC_PCL, SRC_PCH, SRC_P, SRC_FF);
    type dst_select_t is (DST_A, DST_X, DST_Y, DST_S, DST_DL, DST_ADH, DST_ADL, DST_PCH, DST_PCL);

    constant DMA_ADDR: unsigned(15 downto 0) := x"4014";
    constant CONTROLLER_BASE_ADDR: unsigned(14 downto 0) := "010" & x"00B";

    constant NES_H: integer := 256;
    constant NES_V: integer := 240;
    
end package common_pkg;

package body common_pkg is
    
end package body common_pkg;