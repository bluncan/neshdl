library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library R2A03;
library debugger;
use R2A03.ALL;
use debugger.all;


entity Nes is
    port (
        clk100: in std_logic;
        rst: in std_logic;
        rx: in std_logic;
        tx: out std_logic;
        cpu_halt: out std_logic;

        controller_clk: out std_logic;
        controller_latch: out std_logic;
        controller1_din: in std_logic;
        --controller2_din: in std_logic;
        hsync: out std_logic;
        vsync: out std_logic;
        red: out unsigned(3 downto 0);
        green: out unsigned(3 downto 0);
        blue: out unsigned(3 downto 0)
    );
end Nes;

architecture Behavioral of Nes is
    -- Clocks
    signal clk_cpu, clk_ppu, clk_vga: std_logic;

    signal cpu_ce: std_logic;
    signal cpu_nmi_n: std_logic;
    signal cpu_irq_n: std_logic;
    signal cpu_din: unsigned(7 downto 0);
    signal cpu_dout_i: unsigned(7 downto 0);
    signal cpu_addr_i: unsigned(15 downto 0);
    signal cpu_we_n_i: std_logic;
    -- Multiplexed with the DMA
    signal cpu_dout: unsigned(7 downto 0);
    signal cpu_addr: unsigned(15 downto 0);
    signal cpu_we_n: std_logic;
    -- Simulation
    signal cpu_sim_a: unsigned(7 downto 0);
    signal cpu_sim_x: unsigned(7 downto 0);
    signal cpu_sim_y: unsigned(7 downto 0);
    signal cpu_sim_sp: unsigned(7 downto 0);
    signal cpu_sim_status: unsigned(7 downto 0);
    signal cpu_sim_pc: unsigned(15 downto 0);
    signal cpu_sim_count: unsigned(31 downto 0);
    signal cpu_sim_write: std_logic;

    -- PPU
    signal ppu_bus_we: std_logic;
    signal ppu_bus_addr: unsigned(13 downto 0);
    signal ppu_bus_dout: unsigned(7 downto 0);
    signal ppu_bus_din: unsigned(7 downto 0);
    signal ppu_din: unsigned(7 downto 0);
    signal ppu_dout: unsigned(7 downto 0);
    signal ppu_rw_reg: std_logic;
    
    -- Work ram
    signal wram_ce: std_logic;
    signal wram_dout: unsigned(7 downto 0);

    -- Vram
    signal vram_ce: std_logic;
    signal vram_dout: unsigned(7 downto 0);
    signal vram_addr: unsigned(10 downto 0);

    -- PRGROM memory
    signal prgrom_ce: std_logic;
    signal prgrom_dout: unsigned(7 downto 0);
    signal prgrom_addr: unsigned(14 downto 0);
    signal prgrom_din: unsigned(7 downto 0);

    -- CHR Pattern Table memory
    signal chram_ce: std_logic;
    signal chram_dout: unsigned(7 downto 0);
    signal chrom_addr: unsigned(12 downto 0);
    signal chrom_din: unsigned(7 downto 0);

    -- Controller
    signal controller_dout: unsigned(7 downto 0);
    signal controller_ce: std_logic;
    signal controller2_din: std_logic;

    -- DMA controller
    signal dma_halt: std_logic;
    signal dma_addr: unsigned(15 downto 0);
    signal dma_dout: unsigned(7 downto 0);
    signal dma_din: unsigned(7 downto 0);
    signal dma_we_n: std_logic;
    
    -- Debugger
    signal debugger_addr: unsigned(15 downto 0);
    signal debugger_dout: unsigned(7 downto 0);
    signal debugger_we: std_logic;
    signal debugger_active: std_logic;
    signal debugger_cpu_rst: std_logic;
    signal ines_header: unsigned(39 downto 0);

begin

    cpu_halt <= not cpu_ce;
    
    clk_gen_c: entity work.clock_gen
        port map (
            clk100 => clk100,
            rst => rst,
            clk_cpu => clk_cpu,
            clk_ppu => clk_ppu,
            clk_vga => clk_vga
        );

    cpu_ce <= (not dma_halt) and (not debugger_active);
    cpu_c: entity work.cpu_r2a03 
        port map (
            clk1_8 => clk_cpu,
            rst => rst or debugger_cpu_rst,
            ce => cpu_ce,
            nmi_n => cpu_nmi_n,
            irq_n => '1',
            din => cpu_din,
            dout => cpu_dout_i,
            addr => cpu_addr_i,
            we_n => cpu_we_n_i,
            -- Simulations
            sim_a => cpu_sim_a,
            sim_x => cpu_sim_x,
            sim_y => cpu_sim_y,
            sim_sp => cpu_sim_sp,
            sim_status => cpu_sim_status,
            sim_pc => cpu_sim_pc,
            sim_count => cpu_sim_count,
            sim_write => cpu_sim_write
        );

    dma_c: entity work.dma
        port map (
            clk => clk_ppu,
            rst => rst,
            addr_in => cpu_addr_i,
            cpu_din => cpu_dout_i,
            cpu_dout => cpu_din,
            cpu_we_n => cpu_we_n_i,
            halt => dma_halt,
            addr_out => dma_addr,
            dout => dma_dout,
            we_n => dma_we_n
        );
    cpu_dout <= dma_dout when dma_halt = '1' else cpu_dout_i;
    cpu_addr <= dma_addr when dma_halt = '1' else cpu_addr_i;
    cpu_we_n <= dma_we_n when dma_halt = '1' else cpu_we_n_i;
    
    controller_ce <= '1' when cpu_addr(15 downto 1) = "010000000001011" else '0';
    controller2_din <= controller1_din;
    controller_c: entity work.controller
            port map (
                clk100 => clk100,
                rst => rst,
                we => not cpu_we_n,
                addr => cpu_addr,
                din => cpu_dout(0),
                controller1_data => controller1_din,
                controller2_data => controller2_din,
                clk => controller_clk,
                latch => controller_latch,
                dout => controller_dout
            );

    -- 2C02 PPU
    ppu_rw_reg <= '1' when cpu_addr(15 downto 13) = "001" else '0';
    ppu_c: entity work.ppu_2c02
        port map (
            clk100 => clk100,
            clk5_1 => clk_ppu,
            clk25 => clk_vga,
            rst => rst,
            din => cpu_dout,
            dout => ppu_dout,
            addr => cpu_addr(2 downto 0),
            we_n => cpu_we_n,
            ce_n => not ppu_rw_reg,
            vbl_n => cpu_nmi_n,
            bus_addr => ppu_bus_addr,
            bus_we => ppu_bus_we,
            bus_din => ppu_din,
            bus_dout => ppu_bus_dout,
            hsync => hsync,
            vsync => vsync,
            red => red,
            green => green,
            blue => blue
        );
    

    -- Work ram $0000 - $1FFF
    -- 2KB ram, mirrored to 1FFFF
    wram_ce <= (not cpu_addr(15)) and (not cpu_addr(14)) and (not cpu_addr(13));
    wram_c: entity work.single_port_ram
        generic map (
            address_width => 11,
            data_width => 8
        )
        port map (
            clk100 => clk100,
            ce => wram_ce,
            we => not cpu_we_n,
            addr => cpu_addr(10 downto 0),
            din => cpu_dout,
            dout => wram_dout
        );

    -- Vram 0x2000 - 0x3FFF (Nametable memory)
    vram_ce <= ppu_bus_addr(13);
    vram_addr <= ppu_bus_addr(11) & ppu_bus_addr(9 downto 0) when ines_header(16) = '0' else
                 ppu_bus_addr(10) & ppu_bus_addr(9 downto 0);
    vram_c: entity work.single_port_ram
        generic map (
            address_width => 11,
            data_width => 8
        )
        port map (
            clk100 => clk100,
            ce => vram_ce,
            we => ppu_bus_we,
            addr => vram_addr,
            din => ppu_bus_dout,
            dout => vram_dout 
        );
    
    -- PRGROM memory 0x8000 - 0xFFFF
    prgrom_addr <= debugger_addr(14 downto 0) when debugger_active = '1' else
                   ('0' & cpu_addr(13 downto 0)) when ines_header(33) = '0' else
                   cpu_addr(14 downto 0);
    prgrom_ce <= debugger_addr(15) when debugger_active = '1' else
                 cpu_addr(15);
    prgrom_din <= debugger_dout when debugger_active = '1' else
                  cpu_dout;
    prgrom_c: entity work.single_port_ram
       generic map (
           address_width => 15,
           data_width => 8
       )
       port map (
           clk100 => clk100,
           ce => prgrom_ce,
           we => (not cpu_we_n) or debugger_we,
           addr => prgrom_addr,
           din => prgrom_din,
           dout => prgrom_dout
       );
    -- prgrom_c: entity work.prgrom_nestest
    --         port map (
    --             clk_in => clk100,
    --             ce => prgrom_ce,
    --             we => (not cpu_we_n) or debugger_we,
    --             addr_a => prgrom_addr, --'0' & cpu_addr(13 downto 0), --'0' & cpu_addr(13 downto 0),
    --             din_a => prgrom_din, --cpu_dout,
    --             dout_a => prgrom_dout
    --         );
    
    -- CHRAM memory 0x0000-0x1FFF
    chrom_addr <= debugger_addr(12 downto 0) when debugger_active = '1' else
                  ppu_bus_addr(12 downto 0);
    chram_ce <= (not debugger_addr(13)) and (not debugger_addr(14)) and (not debugger_addr(15)) when debugger_active = '1' else
                not ppu_bus_addr(13);
    chrom_din <= debugger_dout when debugger_active = '1' else
                 ppu_bus_dout;
    chrom_c: entity work.single_port_ram
       generic map (
           address_width => 13,
           data_width => 8
       )
       port map (
           clk100 => clk100,
           ce => chram_ce,
           we => debugger_we,
           addr => chrom_addr,
           din => chrom_din,
           dout => chram_dout
       );
--   chram_c: entity work.chrom_nestest
--             port map(
--                 clk_in => clk100,
--                 we => debugger_we,
--                 ce => chram_ce,
--                 addr_a => chrom_addr, --ppu_bus_addr(12 downto 0),
--                 din_a => chrom_din, --ppu_bus_dout,
--                 dout_a => chram_dout
--             );

    ppu_din <= chram_dout when chram_ce = '1' else 
               vram_dout when vram_ce = '1' else
               (others => '0');
    
    
    cpu_din <= prgrom_dout when prgrom_ce = '1' else 
               wram_dout when wram_ce = '1' else 
               ppu_dout when ppu_rw_reg = '1' else
               controller_dout when controller_ce = '1' else
               (others => '0');

    debugger_c: entity work.nesdbg
        port map (
            clk100 => clk100,
            rst => rst,
            we => debugger_we,
            addr_out => debugger_addr,
            dout => debugger_dout,
            active => debugger_active,
            cpu_rst => debugger_cpu_rst,
            ines_header => ines_header,
            rx => rx,
            tx => tx
        );

end Behavioral;