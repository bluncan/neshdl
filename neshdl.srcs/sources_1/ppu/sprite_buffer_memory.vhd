library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity sprite_buffer_memory is
    port (
        clk: in std_logic;
        rst: in std_logic;
        ce: in std_logic;
        load: in std_logic;
        din: in unsigned(27 downto 0);
        sb_primary: out std_logic;
        sb_priority: out std_logic;
        sb_ps: out unsigned(1 downto 0);
        sb_pd0: out unsigned(7 downto 0);
        sb_pd1: out unsigned(7 downto 0);
        sb_x: out unsigned(7 downto 0)
    );
end entity sprite_buffer_memory;

architecture rtl of sprite_buffer_memory is
-- SBM: Sprite Buffer Memory
--
-- bits     desc
-- -------  -----
--      27  primary object flag (is sprite 0?)
--      26  priority
-- 25 - 24  palette select (bit 3-2)
-- 23 - 16  pattern data bit 1
-- 15 -  8  pattern data bit 0
--  7 -  0  x-start
    signal q_sbm: unsigned(27 downto 0);
begin
    
    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                q_sbm <= (others => '0');
            elsif ce = '1' then
                if load = '1' then
                    q_sbm <= din;
                end if;
            end if;
         end if;
    end process;
    
    sb_primary <= q_sbm(27);
    sb_priority <= q_sbm(26);
    sb_ps <= q_sbm(25 downto 24);
    sb_pd1 <= q_sbm(23 downto 16);
    sb_pd0 <= q_sbm(15 downto 8);
    sb_x <= q_sbm(7 downto 0);
    
end architecture rtl;