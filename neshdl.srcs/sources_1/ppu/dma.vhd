library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity dma is
    port (
        clk:  in std_logic;
        rst:  in std_logic;
        addr_in: in unsigned(15 downto 0);
        cpu_din:  in unsigned(7 downto 0);
        cpu_dout: in unsigned(7 downto 0);
        cpu_we_n: in std_logic;
        halt: out std_logic;
        addr_out: out unsigned(15 downto 0);
        dout: out unsigned(7 downto 0);
        we_n: out std_logic
    );
end entity dma;

architecture rtl of dma is
    type state_t is (IDLE, READ, WRITE, WAITING);
    signal q_t, d_t: state_t;
    signal q_base_addr, d_base_addr: unsigned(7 downto 0);
    signal q_offset, d_offset: unsigned(7 downto 0);
    signal q_counter, d_counter: unsigned(1 downto 0);
    signal q_data, d_data: unsigned(7 downto 0);

begin
    
    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                q_t <= IDLE;
                q_base_addr <= (others => '0');
                q_data <= (others => '0');
                q_offset <= (others => '0');
            else
                q_t <= d_t;
                q_base_addr <= d_base_addr;
                q_data <= d_data;
                q_offset <= d_offset;
            end if;
        end if;
    end process;

    process(all)
    begin
        d_t <= q_t;
        d_base_addr <= q_base_addr;
        d_data <= q_data;
        d_offset <= q_offset;

        addr_out <= (others => '0');
        dout <= (others => '0');
        we_n <= '1';
        halt <= '0';

        case q_t is
            when IDLE => 
                if addr_in = x"4014" and cpu_we_n = '0' then
                    d_t <= READ;
                    d_base_addr <= cpu_din;
                    d_offset <= (others => '0');
                    halt <= '1';
                end if;
            
            when READ =>
                halt <= '1';
                addr_out <= q_base_addr & q_offset;
                d_data <= cpu_dout;
                d_t <= WRITE;

            when WRITE =>
                halt <= '1';
                addr_out <= x"2004";
                dout <= q_data;
                we_n <= '0';
                if q_offset = x"FF" then
                    d_t <= WAITING;
                else
                    d_offset <= q_offset + 1;
                    d_t <= READ;
                end if;
            
            when WAITING =>
                if cpu_we_n = '1' then
                    d_t <= IDLE;
                end if;
        end case;

    end process;
end architecture rtl;