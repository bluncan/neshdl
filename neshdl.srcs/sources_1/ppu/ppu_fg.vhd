library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity ppu_fg_2c02 is
    port (
        clk: in std_logic;
        rst: in std_logic;
        ce:  in std_logic;
        show_spirte_leftmost: in std_logic;
        sp16:                 in std_logic;
        sp8_addr:             in std_logic;
        oam_addr:             in unsigned(7 downto 0);
        oam_din:              in unsigned(7 downto 0);
        oam_we:               in std_logic;
        x:                    in unsigned(8 downto 0);
        y:                    in unsigned(8 downto 0);
        bus_din:              in unsigned(7 downto 0);
        oam_dout:             out unsigned(7 downto 0);
        overflow:             out std_logic;
        pallete_sel:          out unsigned(3 downto 0);
        primary:              out std_logic;
        priority:             out std_logic;
        bus_addr:             out unsigned(13 downto 0);
        bus_request:          out std_logic
    );
end entity ppu_fg_2c02;


architecture rtl of ppu_fg_2c02 is
    signal q_obj_count, d_obj_count: unsigned(3 downto 0);
    signal q_overflow, d_overflow: std_logic;

    signal sm_din: unsigned(23 downto 0);
    signal sm_we: std_logic;
    signal sm_primary: std_logic;
    signal sm_id: unsigned(7 downto 0);
    signal sm_x: unsigned(7 downto 0);
    signal sm_ps: unsigned(1 downto 0);
    signal sm_priority: std_logic;
    signal sm_h_inv: std_logic;
    signal sm_row: unsigned(3 downto 0);
    signal sm_rst: std_logic;

    type primary_t  is array(0 to 7) of std_logic;
    type priority_t is array(0 to 7) of std_logic;
    type ps_t       is array(0 to 7) of unsigned(1 downto 0);
    type pd1_t      is array(0 to 7) of unsigned(7 downto 0);
    type pd0_t      is array(0 to 7) of unsigned(7 downto 0);
    type x_t        is array(0 to 7) of unsigned(7 downto 0);

    signal sbm_din: unsigned(27 downto 0);
    signal sbm_ce: std_logic;
    signal sbm_rd_primary: primary_t;
    signal sbm_rd_priority: priority_t;
    signal sbm_rd_ps:          ps_t;
    signal sbm_rd_pd1:         pd1_t;
    signal sbm_rd_pd0:         pd0_t;
    signal sbm_rd_x:           x_t;

    signal oam_y: unsigned(7 downto 0);
    signal oam_id: unsigned(7 downto 0);
    signal oam_v_inv: std_logic;
    signal oam_h_inv: std_logic;
    signal oam_priority: std_logic;
    signal oam_ps: unsigned(1 downto 0);
    signal oam_x: unsigned(7 downto 0);

    signal q_pd1_shifter: std_logic_vector(0 to 7);
    signal q_pd0_shifter: std_logic_vector(0 to 7);
    signal q_pd0, d_pd0: unsigned(7 downto 0);
    signal q_pd1, d_pd1: unsigned(7 downto 0);
    
    signal load_shift_registers: std_logic_vector(0 to 7);
    signal shift_registers_ce: std_logic;
    signal load_sbm: std_logic_vector(0 to 7);
    signal obj_cmp: unsigned(8 downto 0);
    signal in_range: std_logic;
    signal primary_priority_pallete: unsigned(5 downto 0);
    signal sprite_index: integer range -1 to 7;

begin

    registers_latch:process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                q_obj_count <= (others => '0');
                q_overflow <= '0';
                q_pd0 <= (others => '0');
                q_pd1 <= (others => '0');
            else
                q_obj_count <= d_obj_count;
                q_overflow <= d_overflow;
                q_pd0 <= d_pd0;
                q_pd1 <= d_pd1;
            end if;
        end if;
    end process registers_latch;
    

    oam_mem: entity work.oam_memory
        port map (
            clk => clk,
            rst => rst,
            we => oam_we,
            addr => oam_addr,
            din => oam_din,
            dout => oam_dout,
            oam_index => x(7 downto 2),
            oam_y => oam_y,
            oam_id => oam_id,
            oam_v_inv => oam_v_inv,
            oam_h_inv => oam_h_inv,
            oam_priority => oam_priority,
            oam_ps => oam_ps,
            oam_x => oam_x
        );


    sm_rst <= '1' when x = 257 else '0';
    sm_din <= oam_id & oam_x & oam_ps & oam_priority & oam_h_inv & obj_cmp(3 downto 0);
    sprite_temp_memory: entity work.sprite_temp_memory
        port map (
            clk => clk,
            rst => rst,
            we => sm_we,
            addr => q_obj_count(2 downto 0),
            index => x(5 downto 3),
            din => sm_din,
            v_inv => oam_v_inv,
            oam_index => x(7 downto 2),
            sm_primary => sm_primary,
            sm_id => sm_id,
            sm_x => sm_x,
            sm_ps => sm_ps,
            sm_priority => sm_priority,
            sm_h_inv => sm_h_inv,
            sm_row => sm_row
        );


    gen_sbm: for i in 0 to 7 generate
        load_sbm(i) <= '1' when to_integer(x(5 downto 3)) = i else '0';
        sprite_buffer_mem: entity work.sprite_buffer_memory
            port map (
                clk => clk,
                rst => rst,
                ce => sbm_ce,
                load => load_sbm(i),
                din => sbm_din,
                sb_primary => sbm_rd_primary(i),
                sb_priority => sbm_rd_priority(i),
                sb_ps => sbm_rd_ps(i),
                sb_pd1 => sbm_rd_pd1(i),
                sb_pd0 => sbm_rd_pd0(i),
                sb_x => sbm_rd_x(i)
            );
    end generate gen_sbm;
    

    shift_registers_ce <= '1' when ce = '1' and y < 239 else '0';
    gen_registers: for i in 0 to 7 generate
        load_shift_registers(i) <= '1' when (x - sbm_rd_x(i)) = 0 else '0';

        shifter_pd0_i: entity work.shift_register
            port map (
                clk => clk,
                rst => rst,
                ce => shift_registers_ce,
                load => load_shift_registers(i),
                din => sbm_rd_pd0(i),
                shift_out => q_pd0_shifter(i)
            );
    
        shifter_pd1_i: entity work.shift_register
            port map (
                clk => clk,
                rst => rst,
                ce => shift_registers_ce,
                load => load_shift_registers(i),
                din => sbm_rd_pd1(i),
                shift_out => q_pd1_shifter(i)
            );
    end generate gen_registers;


    obj_cmp <= (y + 1 - oam_y) when y /= 260 else "111111111" - oam_y;
    in_range <= '1' when ((obj_cmp(8 downto 4) = 0) and ((not obj_cmp(3)) or sp16) = '1') else '0';

    process(all)
    begin
        d_obj_count <= q_obj_count;
        sm_we <= '0';
        d_pd0 <= q_pd0;
        d_pd1 <= q_pd1;
        sbm_ce <= '0';
        sbm_din <= (others => '0');
        bus_request <= '0';

        if y = "111111111" and x = 0 then
            d_overflow <= '0';
        else
            d_overflow <= q_overflow or q_obj_count(3);
        end if;
        
        if ce = '1' and y < 238 then
            if x >= 256 and x < 320 then
                if x(5 downto 3) < q_obj_count then
                    case x(2 downto 1) is
                        when "00" =>
                            bus_request <= '1';
                            if sm_h_inv = '1' then
                                d_pd0 <= bus_din;
                            else
                                for i in 0 to 7 loop
                                    d_pd0(i) <= bus_din(7-i);
                                end loop;
                            end if;
                        when "01" =>
                            bus_request <= '1';
                            if sm_h_inv = '1' then
                                d_pd1 <= bus_din;
                            else
                                for i in 0 to 7 loop
                                    d_pd1(i) <= bus_din(7-i);
                                end loop;
                            end if;
                        when "10" =>
                            sbm_din <= sm_primary & sm_priority & sm_ps & q_pd1 & q_pd0 & sm_x;
                            sbm_ce <= '1';
                        when others => null;
                    end case;
                else
                    sbm_ce <= '1';
                end if;
            elsif x = 320 then
                d_obj_count <= (others => '0');
            elsif (x < 256) and (x(1 downto 0) = 0) and (in_range = '1') and (q_obj_count(3) = '0') then
                sm_we <= '1';
                d_obj_count <= q_obj_count + 1;
            end if;
        end if;
    end process;


    priority_encoder: process(all)
    begin
        sprite_index <= -1;
        if ce = '0' or (show_spirte_leftmost = '0' and x < 8) then
            sprite_index <= -1;
        elsif q_pd1_shifter(0) = '1' or q_pd0_shifter(0) = '1' then
            sprite_index <= 0;
        elsif q_pd1_shifter(1) = '1' or q_pd0_shifter(1) = '1' then
            sprite_index <= 1;
        elsif q_pd1_shifter(2) = '1' or q_pd0_shifter(2) = '1' then
            sprite_index <= 2;
        elsif q_pd1_shifter(3) = '1' or q_pd0_shifter(3) = '1' then
            sprite_index <= 3;
        elsif q_pd1_shifter(4) = '1' or q_pd0_shifter(4) = '1' then
            sprite_index <= 4;
        elsif q_pd1_shifter(5) = '1' or q_pd0_shifter(5) = '1' then
            sprite_index <= 5;
        elsif q_pd1_shifter(6) = '1' or q_pd0_shifter(6) = '1' then
            sprite_index <= 6;
        elsif q_pd1_shifter(7) = '1' or q_pd0_shifter(7) = '1' then
            sprite_index <= 7;
        end if;
    end process;
    
    primary <= '0' when sprite_index = -1 else sbm_rd_primary(sprite_index);
    priority <= '0' when sprite_index = -1 else sbm_rd_priority(sprite_index);
    pallete_sel <= "0000" when sprite_index = -1 else sbm_rd_ps(sprite_index) & q_pd1_shifter(sprite_index) & q_pd0_shifter(sprite_index);

    with sp16 select
        bus_addr <= '0' & sm_id(0) & sm_id(7 downto 1) & sm_row(3) & x(1) & sm_row(2 downto 0) when '1',
                    '0' & sp8_addr & sm_id & x(1) & sm_row(2 downto 0)                         when '0',
                    "00000000000000"                                                           when others;
    overflow <= q_overflow;

end architecture rtl;