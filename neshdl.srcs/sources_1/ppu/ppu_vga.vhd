library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

use work.common_pkg.all;

entity ppu_2c02_vga is
    port (
        clk25:  in std_logic;
        rst:     in std_logic;
        pal_sel: in unsigned(5 downto 0);
        hsync:  out std_logic;
        vsync:  out std_logic;
        x:      out unsigned(9 downto 0);
        y:      out unsigned(9 downto 0);
        red:    out unsigned(3 downto 0);
        green:  out unsigned(3 downto 0);
        blue:   out unsigned(3 downto 0)
    );
end entity ppu_2c02_vga;

architecture rtl of ppu_2c02_vga is
    
    constant H_BORDER: integer := 80;
    constant V_BORDER: integer := 00;

    signal q_red, d_red: unsigned(3 downto 0);
    signal q_green, d_green: unsigned(3 downto 0);
    signal q_blue, d_blue: unsigned(3 downto 0); 
    signal x_i, y_i : unsigned(9 downto 0);
    signal x_out, y_out: unsigned(9 downto 0);
    signal render: std_logic;
    signal rgb: unsigned(11 downto 0);

    type pallete_colors_t is array(0 to 63) of unsigned(11 downto 0);

    -- Original pallete converted taking the 4 msb bits of each component (r,g,b).
    signal pallete_colors: pallete_colors_t:= (
        x"555",
        x"017",
        x"019",
        x"308",
        x"406",
        x"503",
        x"500",
        x"310",
        x"220",
        x"030",
        x"040",
        x"030",
        x"033",
        x"000",
        x"000",
        x"000",
        x"999",
        x"04c",
        x"33e",
        x"51e",
        x"81b",
        x"a16",
        x"922",
        x"730",
        x"550",
        x"270",
        x"070",
        x"072",
        x"067",
        x"000",
        x"000",
        x"000",
        x"eee",
        x"49e",
        x"77e",
        x"b6e",
        x"e5e",
        x"e5b",
        x"e66",
        x"d82",
        x"aa0",
        x"7c0",
        x"4d2",
        x"3c6",
        x"3bc",
        x"333",
        x"000",
        x"000",
        x"eee",
        x"ace",
        x"bbe",
        x"dbe",
        x"eae",
        x"ead",
        x"ebb",
        x"ec9",
        x"cd7",
        x"bd7",
        x"ae9",
        x"9eb",
        x"ade",
        x"aaa",
        x"000",
        x"000"
    );

begin
    
vga_c: entity work.vga_core
    port map (
        clk25 => clk25,
        rst => rst,
        hsync => hsync,
        vsync => vsync,
        x  => x_i,
        y => y_i
    );

    process(clk25)
    begin
        if rising_edge(clk25) then
            if rst = '1' then
                q_red <= (others => '0');
                q_blue <= (others => '0');
                q_green <= (others => '0');
            else
                q_red <= d_red;
                q_blue <= d_blue;
                q_green <= d_green;
            end if;
        end if;
    end process;

    process(all)
    begin
        if render = '0' then
            d_red <= "0001";
            d_green <= "0001";
            d_blue <= "0001";
        else
            d_red <= pallete_colors(to_integer(pal_sel))(11 downto 8);
            d_green <= pallete_colors(to_integer(pal_sel))(7 downto 4);
            d_blue <= pallete_colors(to_integer(pal_sel))(3 downto 0);
        end if;
    end process;

    x_out <= (x_i - H_BORDER) / 2;
    y_out <= (y_i - V_BORDER) / 2;

    red <= q_red;
    green <= q_green;
    blue <= q_blue;
    x <= x_out;
    y <= y_out;

    -- hide "dangerous" locations: first 8 pixels from left, right, top and bottom to mask scrolling artifacts
    render <= '1' when (x_out >= 8 and x_out < (NES_H - 8) and y_out >= 8 and y_out <= (NES_V - 8)) else '0';
    
end architecture rtl;
