library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity sprite_temp_memory is
    port (
        clk: in std_logic;
        rst: in std_logic;
        we: in std_logic;
        addr: in unsigned(2 downto 0);
        index: in unsigned(2 downto 0);
        din: in unsigned(23 downto 0);
        v_inv: in std_logic;
        oam_index: in unsigned(5 downto 0);
        sm_primary: out std_logic;
        sm_id: out unsigned(7 downto 0);
        sm_x: out unsigned(7 downto 0);
        sm_ps: out unsigned(1 downto 0);
        sm_priority: out std_logic;
        sm_h_inv: out std_logic;
        sm_row: out unsigned(3 downto 0)
    );
end entity sprite_temp_memory;

architecture rtl of sprite_temp_memory is
-- STM: Sprite Temporary Memory
--
-- bits     desc
-- -------  -----
--      24  primary object flag (is sprite 0?)
-- 23 : 16  tile index
-- 15 :  8  x coordinate
--  7 :  6  palette select bits
--       5  object priority
--       4  apply bit reversal to fetched object pattern table data (horizontal invert)
--  3 :  0  range comparison result (sprite row)
    type sprite_t is array(0 to 7) of unsigned(24 downto 0);
    signal sprite_memory_i: sprite_t;
    signal din_i: unsigned(24 downto 0);
    signal idx: integer range 0 to 7;

begin
    
    din_i(23 downto 4) <= din(23 downto 4);
    din_i(3 downto 0) <= din(3 downto 0) when v_inv = '0' else not din(3 downto 0);
    din_i(24) <= '0' when oam_index /= 0 else '1';

    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                for i in 0 to 7 loop
                    sprite_memory_i(i) <= (others => '1');
                end loop;
            elsif we = '1' then
                sprite_memory_i(to_integer(addr)) <= din_i;
            end if;
        end if;
    end process;

    idx <= to_integer(index);
    sm_primary <= sprite_memory_i(idx)(24);
    sm_id <= sprite_memory_i(idx)(23 downto 16);
    sm_x <= sprite_memory_i(idx)(15 downto 8);
    sm_ps <= sprite_memory_i(idx)(7 downto 6);
    sm_priority <= sprite_memory_i(idx)(5);
    sm_h_inv <= sprite_memory_i(idx)(4);
    sm_row <= sprite_memory_i(idx)(3 downto 0);
    
end architecture rtl;