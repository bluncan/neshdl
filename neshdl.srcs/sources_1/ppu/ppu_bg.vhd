library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity ppu_bg_2c02 is
    port (
        clk: in std_logic;
        rst: in std_logic;
        ce: in std_logic;

        x:     in unsigned(8 downto 0);      -- X coordonate (pixel)
        y:  in unsigned(8 downto 0);         -- Y coordonate (scanline)
        din: in unsigned(7 downto 0);            -- Vram data in
        bg_addr: in std_logic;                   -- Background pattern table address (0: $0000; 1: $1000)
        vram_inc: in std_logic;                  -- VRAM address increment per CPU read/write of PPUDATA (0: add 1, going across; 1: add 32, going down)
        show_bg_leftmost: in std_logic;          -- Show background in leftmost 8 pixels of screen, 0: Hide
        fine_x: in unsigned(2 downto 0);         -- Fine X scroll
        t:      in unsigned(14 downto 0);
        update_v: in std_logic;                  -- Update the current VRAM address (loopy register)
        increment_v: in std_logic;               -- Increment the VRAM address
        
        bg_pallete_sel: out unsigned(3 downto 0);
        v: out unsigned(14 downto 0);
        addr: out unsigned(13 downto 0)
    );
end entity ppu_bg_2c02;

architecture behavioral of ppu_bg_2c02 is

    type address_select_t is (
        ADDR_VRAM,
        ADDR_NT,
        ADDR_ATTR,
        ADDR_PTR_LOW,
        ADDR_PTR_HIGH
    );

    signal q_address_select, d_address_select: address_select_t;
    signal q_loopy_v, d_loopy_v: unsigned(14 downto 0);
    
    -- Aliases for easy access
    alias q_fine_y:    unsigned(2 downto 0) is q_loopy_v(14 downto 12);      -- Fine scroll Y
    alias q_nametable_sel: unsigned(1 downto 0) is q_loopy_v(11 downto 10);  -- Nametable select
    alias q_coarse_y:  unsigned(4 downto 0) is q_loopy_v(9 downto 5);        -- Coarse Y scroll
    alias q_coarse_x:  unsigned(4 downto 0) is q_loopy_v(4 downto 0);        -- Coarse X scroll
    
    alias d_fine_y:    unsigned(2 downto 0) is d_loopy_v(14 downto 12);
    alias d_nametable_sel: unsigned(1 downto 0) is d_loopy_v(11 downto 10);
    alias d_coarse_y:  unsigned(4 downto 0) is d_loopy_v(9 downto 5);
    alias d_coarse_x:  unsigned(4 downto 0) is d_loopy_v(4 downto 0);

    
    -- Internal registers
    signal q_nametable_id, d_nametable_id: unsigned(7 downto 0);
    signal q_attribute, d_attribute: unsigned(1 downto 0);
    signal q_pattern_low, d_pattern_low: unsigned(7 downto 0);
    signal q_pattern_high, d_pattern_high: unsigned(7 downto 0);

    signal q_shifter_attrib_high, d_shifter_attrib_high: unsigned(7 downto 0);
    signal q_shifter_attrib_low, d_shifter_attrib_low: unsigned(7 downto 0);
    signal q_shifter_pattern_high, d_shifter_pattern_high: unsigned(15 downto 0);
    signal q_shifter_pattern_low, d_shifter_pattern_low: unsigned(15 downto 0);
    signal q_current_attrbiute, d_current_attrbiute: unsigned(1 downto 0);

    -- Control signals
    signal load_y: std_logic;
    signal load_x: std_logic;
    signal increment_y: std_logic;
    signal increment_x: std_logic;
    signal shift_ammt: integer range 0 to 7;
    signal load_shifters: std_logic;
    signal shift_shifters: std_logic;

begin

    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                q_address_select <= ADDR_VRAM;
                q_loopy_v <= (others => '0');
                q_nametable_id <= (others => '0');
                q_attribute <= (others => '0');
                q_pattern_low <= (others => '0');
                q_pattern_high <= (others => '0');
                q_shifter_pattern_low <= (others => '0');
                q_shifter_pattern_high <= (others => '0');
                q_shifter_attrib_low <= (others => '0');
                q_shifter_attrib_high <= (others => '0');
                q_current_attrbiute <= (others => '0');
            else
                q_address_select <= d_address_select;
                q_loopy_v <= d_loopy_v;
                q_nametable_id <= d_nametable_id;
                q_attribute <= d_attribute;
                q_pattern_low <= d_pattern_low;
                q_pattern_high <= d_pattern_high;
                q_shifter_pattern_low <= d_shifter_pattern_low;
                q_shifter_pattern_high <= d_shifter_pattern_high;
                q_shifter_attrib_low <= d_shifter_attrib_low;
                q_shifter_attrib_high <= d_shifter_attrib_high;
                q_current_attrbiute <= d_current_attrbiute;
            end if;
        end if;
    end process;

    process(all)
        variable v_counter_inc: unsigned(8 downto 0) := (q_nametable_sel(1) & q_coarse_y & q_fine_y) + 1;
        variable h_counter_inc: unsigned(5 downto 0) := (q_nametable_sel(0) & q_coarse_x) + 1;
    begin
        d_loopy_v <= q_loopy_v;
        d_shifter_pattern_low <= q_shifter_pattern_low;
        d_shifter_pattern_high <= q_shifter_pattern_high;
        d_shifter_attrib_low <= q_shifter_attrib_low;
        d_shifter_attrib_high <= q_shifter_attrib_high;
        d_current_attrbiute <= q_current_attrbiute;

        if increment_v = '1' then
            if vram_inc = '0' then
                d_loopy_v <= q_loopy_v + 1;
            else
                d_loopy_v <= q_loopy_v + 32;
            end if;
        else
            if increment_y = '1' then
                if q_coarse_y = 29 and q_fine_y = 7 then
                    d_nametable_sel(1) <= not q_nametable_sel(1);
                    d_coarse_x <= (others => '0');
                    d_coarse_y <= (others => '0');
                else
                    d_nametable_sel(1) <= v_counter_inc(8);
                    d_coarse_y <= v_counter_inc(7 downto 3);
                    d_fine_y <= v_counter_inc(2 downto 0);
                end if;
            end if;

            if increment_x = '1' then
                d_nametable_sel(0) <= h_counter_inc(5);
                d_coarse_x <= h_counter_inc(4 downto 0);
            end if;

            if update_v = '1' then
                d_loopy_v <= t;
            else
                if load_y = '1' then
                    d_nametable_sel(1) <= t(11);
                    d_coarse_y <= t(9 downto 5);
                    d_fine_y <= t(14 downto 12);
                end if;
                
                if load_x = '1' then
                    d_nametable_sel(0) <= t(10);
                    d_coarse_x <= t(4 downto 0);
                end if;
            end if;
        end if;

        if ce = '1' then
            if shift_shifters = '1' then
                d_shifter_attrib_high <= q_current_attrbiute(1) & q_shifter_attrib_high(7 downto 1);
                d_shifter_attrib_low <= q_current_attrbiute(0) & q_shifter_attrib_low(7 downto 1);
                d_shifter_pattern_high <= '0' & q_shifter_pattern_high(15 downto 1);
                d_shifter_pattern_low <= '0' & q_shifter_pattern_low(15 downto 1);
            end if;

            if load_shifters = '1' then
                d_current_attrbiute <= q_attribute;
                gen_bit_1: for i in 0 to 7 loop
                    d_shifter_pattern_high(15-i) <= q_pattern_high(i);
                end loop;
                gen_bit_0: for i in 0 to 7 loop
                    d_shifter_pattern_low(15-i) <= q_pattern_low(i);
                end loop;
            end if;
        end if;
    end process;


    process(all)
        variable shift_by: unsigned(2 downto 0) := q_coarse_y(1) & q_coarse_x(1) & '0';
    begin
        d_nametable_id <= q_nametable_id;
        d_attribute <= q_attribute;
        d_pattern_low <= q_pattern_low;
        d_pattern_high <= q_pattern_high;
        d_address_select <= q_address_select;

        load_y <= '0';
        load_x <= '0';
        increment_x <= '0';
        increment_y <= '0';
        load_shifters <= '0';
        shift_shifters <= '0';
        increment_x <= '0';
        
        if ce = '1' then
            if y = "111111111" or y < 239 then
                if (x >= 2 and x < 258) or (x >= 321 and x < 338) then
                    shift_shifters <= '1';
                    case (x(2 downto 0)) is
                        when "000" =>
                            load_shifters <= '1';
                            d_address_select <= ADDR_NT;
                        when "001" =>
                            d_nametable_id <= din;
                        when "010" =>
                            d_address_select <= ADDR_ATTR;
                        when "011" =>
                            d_attribute <= shift_right(din, to_integer(shift_by))(1 downto 0);
                        when "100" =>
                            d_address_select <= ADDR_PTR_LOW;
                        when "101" =>
                            d_pattern_low <= din;
                        when "110" =>
                            d_address_select <= ADDR_PTR_HIGH;
                        when "111" =>
                            d_pattern_high <= din;
                            increment_x <= '1';
                        when others => null;
                    end case;
                end if;
                
                if x = 256 then
                    increment_y <= '1';
                    increment_x <= '1';
                elsif x = 257 then
                    load_shifters <= '1';
                    load_x <= '1';
                elsif x = 337 then
                    d_address_select <= ADDR_NT;
                elsif x = 338 then
                    d_nametable_id <= din;
                elsif x = 339 then
                    d_address_select <= ADDR_NT;
                elsif x = 340 then
                    d_nametable_id <= din;
                end if;
                
                if y = "111111111" and x >= 280 and x < 305 then
                    load_y <= '1';
                end if;
            else
                d_address_select <= ADDR_VRAM;
            end if;
        else
            d_address_select <= ADDR_VRAM;
        end if;
    end process;


    with q_address_select select
        addr <= "10" & q_nametable_sel & q_coarse_y & q_coarse_x when ADDR_NT,
                "10" & q_nametable_sel & "1111" & q_coarse_y(4 downto 2) & q_coarse_x(4 downto 2) when ADDR_ATTR,
                '0' & bg_addr & q_nametable_id & '0' & q_fine_y when ADDR_PTR_LOW,
                '0' & bg_addr & q_nametable_id & '1' & q_fine_y when ADDR_PTR_HIGH,
                q_loopy_v(13 downto 0) when ADDR_VRAM;
    
    shift_ammt <= to_integer(fine_x);
    bg_pallete_sel <= "0000" when ce = '0' or (show_bg_leftmost = '0' and x < 8) else
                      q_shifter_attrib_high(shift_ammt) & q_shifter_attrib_low(shift_ammt) & 
                      q_shifter_pattern_high(shift_ammt) & q_shifter_pattern_low(shift_ammt);
    v <= q_loopy_v;

end architecture behavioral;