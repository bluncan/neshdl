library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity oam_memory is
    port (
        clk:    in std_logic;
        rst:    in std_logic;
        we:     in std_logic;
        addr:   in unsigned(7 downto 0);
        din:    in unsigned(7 downto 0);
        oam_index:  in unsigned(5 downto 0);
        oam_y:  out unsigned(7 downto 0);
        oam_id: out unsigned(7 downto 0);
        oam_h_inv: out std_logic;
        oam_v_inv: out std_logic;
        oam_priority: out std_logic;
        oam_ps: out unsigned(1 downto 0);
        oam_x:  out unsigned(7 downto 0);
        dout:  out unsigned(7 downto 0)
    );
end entity oam_memory;

architecture rtl of oam_memory is

-- OAM: Object Attribute Memory (256 entries, 4 bytes per entry)
--
-- byte bits desc
-- ---- ---- ----
--  0   7:0  scanline coordinate minus one of object's top pixel row.
--  1   7:0  tile index number. Bit 0 here controls pattern table selection when reg 0x2000[5]5 = 1.
--  2     0  palette select low bit
--        1  palette select high bit
--        5 object priority (> playfield's if 0; < playfield's if 1)
--        6 apply bit reversal to fetched object pattern table data
--        7 invert the 3/4-bit (8/16 scanlines/object mode) scanline address used to access an object
--          tile
--  3   7:0 scanline pixel coordinate of most left-hand side of object.
    type oam_t is array(0 to 255) of unsigned(7 downto 0);
    signal oam_memory_i: oam_t;
    signal index: integer range 0 to 255;
    
begin
    
    index <= to_integer(oam_index);
    
    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                for i in 0 to 255 loop
                    oam_memory_i(i) <= (others => '0');
                end loop;
            elsif we = '1' then
                oam_memory_i(to_integer(addr)) <= din;
            end if;
        end if;
    end process;

    dout <= oam_memory_i(to_integer(addr));

    oam_y <= oam_memory_i(index * 4 + 0) + 1;
    oam_id <= oam_memory_i(index * 4 + 1);
    oam_v_inv <= oam_memory_i(index * 4 + 2)(7);
    oam_h_inv <= oam_memory_i(index * 4 + 2)(6);
    oam_priority <= oam_memory_i(index * 4 + 2)(5);
    oam_ps <= oam_memory_i(index * 4 + 2)(1 downto 0);
    oam_x <= oam_memory_i(index * 4 + 3);


end architecture rtl;