library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- https://www.nesdev.com/2C02%20technical%20reference.TXT
entity ppu_2c02 is 
    port (
        clk100:  in std_logic;  -- 100 MHz system clock
        clk5_1:  in std_logic;  -- 5.1 MHz clock
        clk25:   in std_logic;  -- 25 MHz ppu clock
        rst   :  in std_logic;  -- System reset
        din   :  in unsigned(7 downto 0);   -- Data input
        dout  : out unsigned(7 downto 0);   -- Data output
        addr  :  in unsigned(2 downto 0);   -- Register select
        we_n  :  in std_logic;              -- Write enable (active low)
        ce_n  :  in std_logic;              -- Chip select (active low)
        vbl_n : out std_logic;              -- 0 during the VBLANK
        -- Bus signals
        bus_addr: out unsigned(13 downto 0);  -- Bus address
        bus_we:   out std_logic;              -- 0 for read, 1 for write
        bus_din:   in unsigned(7 downto 0);   -- Data from the bus
        bus_dout: out unsigned(7 downto 0);   -- Data to bus
        -- VGA signals
        hsync: out std_logic;
        vsync: out std_logic;
        red: out unsigned(3 downto 0);
        green: out unsigned(3 downto 0);
        blue: out unsigned(3 downto 0)
    );
end entity ppu_2c02;

architecture behavioral of ppu_2c02 is
    -- ##### External registers #####
    signal q_ppu_ctrl: unsigned(7 downto 0);   -- PPU control register $2000
    signal d_ppu_ctrl: unsigned(7 downto 0);
    -- PPU control aliases for easy access
    alias q_nmi_enable:       std_logic is q_ppu_ctrl(7); -- Generate an NMI at the start of the vertical blanking interval (0: off; 1: on)
    alias q_ms_select:        std_logic is q_ppu_ctrl(6); -- PPU master/slave select (0: read backdrop from EXT pins; 1: output color on EXT pins)
    alias q_sp16:             std_logic is q_ppu_ctrl(5); -- Sprite size (0: 8x8 cycles; 1: 8x16 cycles)
    alias q_bg_addr:          std_logic is q_ppu_ctrl(4); -- Background pattern table address (0: $0000; 1: $1000)
    alias q_sp8_addr:         std_logic is q_ppu_ctrl(3); -- Sprite pattern table address for 8x8 sprites (0: $0000; 1: $1000; ignored in 8x16 mode)
    alias q_vram_inc:         std_logic is q_ppu_ctrl(2); -- VRAM address increment per CPU read/write of PPUDATA (0: add 1, going across; 1: add 32, going down)
    alias q_nametable_addr:   unsigned(1 downto 0) is q_ppu_ctrl(1 downto 0); --Base nametable address (0 = $2000; 1 = $2400; 2 = $2800; 3 = $2C00)
    
    alias d_nmi_enable:       std_logic is d_ppu_ctrl(7); 
    alias d_ms_select:        std_logic is d_ppu_ctrl(6); 
    alias d_sp16:             std_logic is d_ppu_ctrl(5); 
    alias d_bg_addr:          std_logic is d_ppu_ctrl(4);
    alias d_sp8_addr:         std_logic is d_ppu_ctrl(3);
    alias d_vram_inc:         std_logic is d_ppu_ctrl(2);
    alias d_nametable_addr:   unsigned(1 downto 0) is d_ppu_ctrl(1 downto 0); --Base nametable address (0 = $2000; 1 = $2400; 2 = $2800; 3 = $2C00)


    signal q_ppu_mask: unsigned(7 downto 0); -- PPU mask register $2001
    signal d_ppu_mask: unsigned(7 downto 0);
    -- PPU mask aliases for easy access
    alias q_emph_blue:              std_logic is q_ppu_mask(7); -- Emphasize blue
    alias q_emph_green:             std_logic is q_ppu_mask(6); -- Emphasize green (red on PAL/Dendy)
    alias q_emph_red:               std_logic is q_ppu_mask(5); -- Emphasize red (green on PAL/Dendy)
    alias q_show_sprite:            std_logic is q_ppu_mask(4); -- 1: Show sprites
    alias q_show_bg:                std_logic is q_ppu_mask(3); -- 1: Show background
    alias q_show_spirte_leftmost:   std_logic is q_ppu_mask(2); -- Show sprites in leftmost 8 cycles of screen, 0: Hide
    alias q_show_bg_leftmost:       std_logic is q_ppu_mask(1); -- Show background in leftmost 8 cycles of screen, 0: Hide
    
    alias q_grayscale:              std_logic is q_ppu_mask(0); -- Greyscale (0: normal color, 1: produce a greyscale display)
    alias d_emph_blue:              std_logic is d_ppu_mask(7); 
    alias d_emph_green:             std_logic is d_ppu_mask(6);
    alias d_emph_red:               std_logic is d_ppu_mask(5);
    alias d_show_sprite:            std_logic is d_ppu_mask(4);
    alias d_show_bg:                std_logic is d_ppu_mask(3);
    alias d_show_spirte_leftmost:   std_logic is d_ppu_mask(2);
    alias d_show_bg_leftmost:       std_logic is d_ppu_mask(1);
    alias d_grayscale:              std_logic is d_ppu_mask(0);

    signal q_ppu_status: unsigned(7 downto 0); -- PPU status register $2002
    signal d_ppu_status: unsigned(7 downto 0);
    -- PPU status aliases for easy access
    alias q_vblank_started:        std_logic is q_ppu_status(7); -- Vertical blank has started (0: not in vblank; 1: in vblank). 
                                                             -- Set at dot 1 of line 241 (the line *after* the post-renderline); 
                                                             --cleared after reading $2002 and at dot 1 of the pre-render line.
    alias q_sp0_hit:              std_logic is q_ppu_status(6);  -- Sprite 0 Hit.  Set when a nonzero cycle of sprite 0 overlaps
                                                             -- a nonzero background cycle; cleared at dot 1 of the pre-renderline.
    alias q_sp_overflow:          std_logic is q_ppu_status(5);  -- This flag is set during sprite evaluation and 
                                                             -- cleared at dot 1 (the second dot) of the pre-render line.
    alias q_unused:               unsigned(4 downto 0) is q_ppu_status(4 downto 0); -- unused
    alias d_vblank_started:       std_logic is d_ppu_status(7);
    alias d_sp0_hit:              std_logic is d_ppu_status(6);
    alias d_sp_overflow:          std_logic is d_ppu_status(5);
    alias d_unused:               unsigned(4 downto 0) is d_ppu_status(4 downto 0);   

    signal d_oam_addr, q_oam_addr: unsigned(7 downto 0);   -- OAM address port $2003
    signal oam_data: unsigned(7 downto 0);   -- OAM data port $2004


    -- ###### Internal registers ######
    signal q_v: unsigned(14 downto 0);             -- Current VRAM address
    signal d_t, q_t: unsigned(14 downto 0);        -- Temporary VRAM address; can also be thought of as the address of the top left onscreen tile.
    signal d_x, q_x: unsigned(2 downto 0);         -- Fine X scroll
    signal d_w, q_w: std_logic;                    -- First or second write toggle
    signal d_read_buf, q_read_buf: unsigned(7 downto 0);   -- Internal read buffer
    signal increment_v: std_logic;
    
    signal q_ce_n: std_logic;
    signal q_update_v, d_update_v: std_logic;      -- True if v should copy the value from t
    
    -- External ports
    signal d_dout, q_dout: unsigned(7 downto 0);

    -- PPU Vga signals
    signal vga_pallete_index: unsigned(5 downto 0);
    signal vga_x: unsigned(9 downto 0);
    signal vga_y: unsigned(9 downto 0);

    -- Bg component
    signal bg_pallete_sel: unsigned(3 downto 0);
    signal bg_addr: unsigned(13 downto 0);

    -- Pallete memory
    signal pallete_we: std_logic;
    signal pallete_addr: unsigned(4 downto 0);
    signal pallete_dout: unsigned(5 downto 0);

    -- Framebuffer 
    signal framebuffer_write_addr: unsigned(15 downto 0);
    signal framebuffer_read_addr: unsigned(15 downto 0);
    signal framebuffer_we: std_logic;
    signal framebuffer_din: unsigned(5 downto 0);

    -- Resoltuion counters
    signal scanline: unsigned(8 downto 0);
    signal cycle:    unsigned(8 downto 0);
    signal odd_frame: std_logic;

    -- Fg componenet
    signal sp_pallete_sel: unsigned(3 downto 0);
    signal sp_primary: std_logic;
    signal sp_priority: std_logic;
    signal sp_overflow: std_logic;
    signal sp_bus_addr: unsigned(13 downto 0);
    signal sp_bus_request: std_logic;
    signal sp_we: std_logic;
    signal oam_dout: unsigned(7 downto 0);

    -- Control signals
    signal set_vblank: std_logic;
    signal clear_vblank: std_logic;
    signal spr_trans: std_logic;
    signal bg_trans: std_logic;
    

begin

    bus_addr <= sp_bus_addr when sp_bus_request = '1' else bg_addr;
    vbl_n <= q_vblank_started nand q_nmi_enable;
    dout <= q_dout;

    spr_trans <= '1' when sp_pallete_sel(1 downto 0) = "00" else '0';
    bg_trans <= '1' when bg_pallete_sel(1 downto 0) = "00" else '0';
    

    framebuffer_we <= '1' when (scanline >= 0 and scanline < 240 and cycle >= 0 and cycle < 256) else '0';
    framebuffer_write_addr <= scanline(7 downto 0) & cycle(7 downto 0);
    framebuffer_read_addr <= vga_y(7 downto 0) & vga_x(7 downto 0);
    framebuffer_c: entity work.framebuffer
        port map (
            clk => clk100,
            we => framebuffer_we and clk5_1,
            write_addr => framebuffer_write_addr,
            read_addr => framebuffer_read_addr,
            din => framebuffer_din,
            dout => vga_pallete_index
        );
    
    pallete_addr <= '1' & sp_pallete_sel when (sp_priority = '0' or bg_trans = '1') and spr_trans = '0' else
                    '0' & bg_pallete_sel when bg_trans = '0' else
                    (others => '0');
    pallete_c: entity work.pallete_memory
        port map (
            clk => clk100,
            we => pallete_we,
            addr => bus_addr(4 downto 0),
            vga_addr => pallete_addr,
            din => bus_dout(5 downto 0),
            dout => pallete_dout,
            vga_dout => framebuffer_din
        );

    ppu_vga_c: entity work.ppu_2c02_vga
        port map (
            clk25 => clk25,
            rst => rst,
            pal_sel => vga_pallete_index,
            hsync => hsync,
            vsync => vsync,
            red => red,
            green => green,
            blue => blue,
            x => vga_x,
            y => vga_y
        );

    ppu_bg_c: entity work.ppu_bg_2c02
        port map (
            clk => clk5_1,
            rst => rst,
            ce => q_show_bg,
            x => cycle,
            y => scanline,
            din => bus_din,
            bg_addr => q_bg_addr,
            vram_inc => q_vram_inc,
            show_bg_leftmost => q_show_bg_leftmost,
            t => q_t,
            fine_x => q_x,
            update_v => q_update_v,
            increment_v => increment_v,
            bg_pallete_sel => bg_pallete_sel,
            v => q_v,
            addr => bg_addr
        );

    ppu_fg_c: entity work.ppu_fg_2c02
        port map (
            clk => clk5_1,
            rst => rst,
            ce => q_show_sprite,
            show_spirte_leftmost => q_show_spirte_leftmost,
            sp16 => q_sp16,
            sp8_addr => q_sp8_addr,
            oam_addr => q_oam_addr,
            oam_din => oam_data,
            oam_we => sp_we,
            x => cycle,
            y => scanline,
            bus_din => bus_din,
            oam_dout => oam_dout,
            overflow => sp_overflow,
            pallete_sel => sp_pallete_sel,
            primary => sp_primary,
            priority => sp_priority,
            bus_addr => sp_bus_addr,
            bus_request => sp_bus_request
        );

        
    registers_latch: process(clk5_1)
    begin
        if rising_edge(clk5_1) then
            if rst = '1' then
                -- External registers
                q_ppu_ctrl <= (others => '0');
                q_ppu_mask <= (others => '0');
                q_ppu_status <= (others => '0');
                q_oam_addr <= (others => '0');
                -- Internal registers
                q_ce_n <= '1';
                q_t <= (others => '0');
                q_w <= '0';
                q_x <= (others => '0');
                q_read_buf <= (others => '0');
                q_update_v <= '0';
                q_dout <= (others => '0');
            else
                -- External registers
                q_ppu_ctrl <= d_ppu_ctrl;
                q_ppu_mask <= d_ppu_mask;
                q_ppu_status <= d_ppu_status;
                q_oam_addr <= d_oam_addr;
                -- Internal registers
                q_t <= d_t;
                q_w <= d_w;
                q_x <= d_x;
                q_read_buf <= d_read_buf;
                q_ce_n <= ce_n;
                q_update_v <= d_update_v;
                q_dout <= d_dout;
            end if;
        end if;
    end process registers_latch;


    register_logic:process(all)
    begin
        d_ppu_ctrl <= q_ppu_ctrl;
        d_ppu_mask <= q_ppu_mask;
        d_ppu_status <= q_ppu_status;
        d_oam_addr <= q_oam_addr;
        d_t <= q_t;
        d_w <= q_w;
        d_x <= q_x;
        d_read_buf <= q_read_buf;
        d_dout <= q_dout;
        d_vblank_started <= q_vblank_started;
        d_sp0_hit <= q_sp0_hit;
        d_sp_overflow <= sp_overflow;

        oam_data <= (others => '0');
        d_update_v <= '0';
        bus_we <= '0';
        sp_we <= '0';
        increment_v <= '0';
        pallete_we <= '0';
        bus_dout <= (others => '0');


        if scanline = "111111111" then
            d_sp0_hit <= '0';
        elsif sp_primary = '1' and spr_trans = '0' and bg_trans = '0' then
            d_sp0_hit <= '1';
        end if;

        if (scanline = "1111111111" or (scanline >= 0 and scanline < 241)) and cycle = 240 then
            d_oam_addr <= (others => '0');
        end if;

        if set_vblank = '1' then
            d_vblank_started <= '1';
        elsif clear_vblank = '1' then
            d_vblank_started <= '0';
        end if;

        -- Falling edge of ce_n
        if q_ce_n = '1' and ce_n = '0' then
            -- Reading registers
            if we_n = '1' then
                case addr is
                    when "010" => -- PPUSTATUS
                        d_dout <= q_ppu_status(7 downto 5) & q_read_buf(4 downto 0);
                        d_vblank_started <= '0';
                        d_w <= '0';
                    
                    when "100" => -- OAMDATA
                        d_dout <= oam_dout;
                    
                    when "111" => -- PPUDATA
                        if q_v(13 downto 8) = "111111" then
                            d_dout <= "00" & pallete_dout;
                        else
                            d_dout <= q_read_buf;
                         end if;
                        d_read_buf <= bus_din; 
                        increment_v <= '1';
                    when others => null;
                end case;
            else
                -- Writing registers
                case addr is
                    when "000" => -- PPUCTRL
                        d_ppu_ctrl <= din;
                        d_t(11 downto 10) <= din(1 downto 0);
                        -- t: ...GH.. ........ <- d: ......GH
                        -- <used elsewhere> <- d: ABCDEF..

                    when "001" => -- PPUMASK
                       d_ppu_mask <= din;
                    
                    when "011" => -- OAMADDR
                        d_oam_addr <= din;

                    when "100" => -- OAMDATA
                        oam_data <= din;
                        sp_we <= '1';
                        d_oam_addr <= q_oam_addr + 1;

                    when "101" => -- PPUSCROLL
                        d_w <= not q_w;
                        if q_w = '0' then
                            -- t: ....... ...ABCDE <- d: ABCDE...
                            -- x:              FGH <- d: .....FGH
                            -- w:                  <- 1
                            d_t(4 downto 0) <= din(7 downto 3);
                            d_x <= din(2 downto 0);
                        else
                            -- t: FGH..AB CDE..... <- d: ABCDEFGH
                            -- w:                  <- 0
                            d_t(14 downto 12) <= din(2 downto 0);
                            d_t(9 downto 5) <= din(7 downto 3);
                            --d_t(7 downto 5) <= din(5 downto 3);
                        end if;

                    when "110" => -- PPUADDR
                        d_w <= not q_w;
                        if q_w = '0' then
                            -- t: .CDEFGH ........ <- d: ..CDEFGH
                            --        <unused>     <- d: AB......
                            -- t: Z...... ........ <- 0 (bit Z is cleared)
                            -- w:                  <- 1
                            d_t(14 downto 8) <= '0' & din(5 downto 0);
                        else
                            -- t: ....... ABCDEFGH <- d: ABCDEFGH
                            -- v: <...all bits...> <- t: <...all bits...>
                            -- w:                  <- 0
                            d_t(7 downto 0) <= din;
                            d_update_v <= '1';
                        end if;

                    when "111" =>
                        if q_v(13 downto 8) = "111111" then
                            pallete_we <= '1';
                        else
                            bus_we <= '1';
                        end if;
                        bus_dout <= din;
                        increment_v <= '1';
                    when others => null;
                end case;
            end if;
        end if;
    end process;

    process(clk5_1)
    begin
        if rising_edge(clk5_1) then
            if rst = '1' then
                odd_frame <= '0';
                scanline <= (others => '1');
                cycle <= (others => '0');
            elsif scanline = 260 and cycle = 340 then
                scanline <= (others => '1');
                cycle <= (others => '0');
            elsif cycle < 340 then
                cycle <= cycle + 1;
            else
                cycle <= (others => '0');
                scanline <= scanline + 1;
            end if;

           if scanline = "111111111" and odd_frame = '1' and cycle = 339 then
               scanline <= (others => '0');
               cycle <= to_unsigned(1, cycle'length);
               odd_frame <= not odd_frame;
           end if;
        end if;
    end process;

    set_vblank <= '1' when (scanline = 241 and cycle = 0) else '0';
    clear_vblank <= '1' when (scanline = "111111111" and cycle =   0 and odd_frame = '0') else 
                    '1' when (scanline = "111111111" and cycle = 339 and odd_frame = '1') else
                    '0';

end architecture behavioral;