library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity framebuffer is
    port (
        clk: in std_logic;
        we:  in std_logic;
        write_addr: in unsigned(15 downto 0);
        read_addr: in unsigned(15 downto 0);
        din: in unsigned(5 downto 0);
        dout: out unsigned(5 downto 0)
        );
end entity framebuffer;

architecture rtl of framebuffer is
    type framebuffer_t is array(0 to 65535) of unsigned(5 downto 0);

    signal framebuffer_ram: framebuffer_t;
    signal q_addr: unsigned(15 downto 0);
begin
    
    process(clk)
    begin
        if rising_edge(clk) then
            if we = '1' then
                framebuffer_ram(to_integer(write_addr)) <= din;
            end if;
            q_addr <= read_addr;
        end if;
    end process;
    
    dout <= framebuffer_ram(to_integer(q_addr));

end architecture rtl;