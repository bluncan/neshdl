library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity shift_register is
    port (
        clk:   in std_logic;
        ce:    in std_logic;
        rst:   in std_logic;
        load:  in std_logic;
        din:   in unsigned(7 downto 0);
        shift_out: out std_logic
    );
end entity shift_register;

architecture rtl of shift_register is
    signal q_reg: unsigned(7 downto 0);
begin
    
    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                q_reg <= (others => '0');
            elsif ce = '1' then
                if load = '1' then
                    q_reg <= din;
                else
                    q_reg <= '0' & q_reg(7 downto 1);
                end if;
            end if;
            shift_out <= q_reg(0);
        end if;
    end process;


end architecture rtl;