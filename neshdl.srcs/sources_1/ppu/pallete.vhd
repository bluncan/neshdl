library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity pallete_memory is
    port (
        clk: in std_logic;
        we: in std_logic;
        addr: in unsigned(4 downto 0);
        vga_addr: in unsigned(4 downto 0);
        din : in unsigned(5 downto 0);
        dout: out unsigned(5 downto 0);
        vga_dout: out unsigned(5 downto 0)
    );
end entity pallete_memory;

architecture rtl of pallete_memory is
    type memory_t is array(0 to 31) of unsigned(5 downto 0);

    signal pallete_ram: memory_t := (
        "100010",
        "101001",
        "011010",
        "001111",
        "100010",
        "110110",
        "010111",
        "001111",
        "100010",
        "110000",
        "100001",
        "001111",
        "100010",
        "100111",
        "010111",
        "001111",
        "000000",
        "010110",
        "100111",
        "011000",
        "000000",
        "011010",
        "110000",
        "100111",
        "000000",
        "010110",
        "110000",
        "100111",
        "000000",
        "001111",
        "110110",
        "010111"
    );

    signal addr_i: unsigned(4 downto 0);
begin
    
    addr_i <= addr when addr(1 downto 0) /= 0 else ('0' & addr(3 downto 0));
        process(clk)
        begin
            if rising_edge(clk) then
                if we = '1' then
                    pallete_ram(to_integer(addr_i)) <= din;
                end if;
            end if;
        end process;
        
    dout <= pallete_ram(to_integer(addr_i));
    vga_dout <= pallete_ram(to_integer(vga_addr));
    
    
    --dout <= pallete_ram(to_integer(addr_i)) when we = '0' else din;
    --vga_dout <= pallete_ram(to_integer(vga_addr));
    
end architecture rtl;