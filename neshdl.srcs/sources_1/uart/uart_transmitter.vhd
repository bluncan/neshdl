library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity UART_TX is
  generic (
    CLK_COUNT : integer := 868
    );
  port (
    clk100      : in  std_logic;
    tx_send     : in  std_logic;
    tx_din      : in  unsigned(7 downto 0);
    tx_active   : out std_logic;
    tx          : out std_logic;
    tx_done     : out std_logic
    );
end UART_TX;


architecture RTL of UART_TX is

  type state_t is (IDLE, TX_START, TX_DATA, TX_STOP, COOLDOWN);
  signal q_t : state_t := IDLE;

  signal q_clk_count : integer range 0 to CLK_COUNT-1 := 0;
  signal q_bit_index : integer range 0 to 7 := 0;
  signal q_tx_data   : unsigned(7 downto 0);
  signal q_tx_done   : std_logic := '0';
  
begin
    process (clk100)
    begin
        if rising_edge(clk100) then
            case q_t is
                when IDLE =>
                    tx_active <= '0';
                    tx <= '1';
                    q_tx_done   <= '0';
                    q_clk_count <= 0;
                    q_bit_index <= 0;
                    if tx_send = '1' then
                        q_tx_data <= tx_din;
                        q_t <= TX_START;
                    else
                        q_t <= IDLE;
                    end if;

                when TX_START =>
                    tx_active <= '1';
                    tx <= '0';
                    if q_clk_count < CLK_COUNT-1 then
                        q_clk_count <= q_clk_count + 1;
                        q_t   <= TX_START;
                    else
                        q_clk_count <= 0;
                        q_t   <= TX_DATA;
                    end if;
        
                when TX_DATA =>
                    tx <= q_tx_data(q_bit_index);
                    if q_clk_count < CLK_COUNT-1 then
                        q_clk_count <= q_clk_count + 1;
                        q_t   <= TX_DATA;
                    else
                        q_clk_count <= 0;
                        if q_bit_index < 7 then
                            q_bit_index <= q_bit_index + 1;
                            q_t   <= TX_DATA;
                        else
                            q_bit_index <= 0;
                            q_t   <= TX_STOP;
                        end if;
                    end if;

                when TX_STOP =>
                    tx <= '1';
                    if q_clk_count < CLK_COUNT-1 then
                        q_clk_count <= q_clk_count + 1;
                        q_t   <= TX_STOP;
                    else
                        q_tx_done   <= '1';
                        q_clk_count <= 0;
                        q_t   <= COOLDOWN;
                    end if;

                when COOLDOWN =>
                    tx_active <= '0';
                    q_tx_done   <= '1';
                    q_t   <= IDLE;
                
                when others =>
                    q_t <= IDLE;
            end case;
        end if;
    end process;

  tx_done <= q_tx_done;
  
end RTL;
