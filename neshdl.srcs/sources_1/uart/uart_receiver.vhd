library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;

entity uart_rx is
  generic (
    CLK_COUNT : integer := 868
    );
  port (
    clk100:    in  std_logic;
    rx:        in  std_logic;
    rx_done:  out std_logic;
    rx_dout:  out unsigned(7 downto 0)
    );
end uart_rx;


architecture RTL of uart_rx is

  type state_t is (IDLE, RX_START, RX_DATA, RX_STOP, COOLDOWN);
  signal q_t : state_t := IDLE;

  signal q_clk_count : integer range 0 to CLK_COUNT-1 := 0;
  signal q_bit_index : integer range 0 to 7;
  signal q_rx_data   : unsigned(7 downto 0) := x"00";
  signal q_rx_done   : std_logic := '0';
  
begin

    process (clk100)
    begin
        if rising_edge(clk100) then
            case q_t is
                when IDLE =>
                    q_rx_done <= '0';
                    q_clk_count <= 0;
                    q_bit_index <= 0;
                    if rx = '0' then  
                        q_t <= RX_START;
                    else
                        q_t <= IDLE;
                    end if;
            
                when RX_START =>
                    if q_clk_count = (CLK_COUNT-1)/2 then
                        if rx = '0' then
                            q_clk_count <= 0;
                            q_t <= RX_DATA;
                        else
                            q_t <= IDLE;
                        end if;
                    else
                        q_clk_count <= q_clk_count + 1;
                        q_t <= RX_START;
                    end if;

                when RX_DATA =>
                    if q_clk_count < CLK_COUNT-1 then
                        q_clk_count <= q_clk_count + 1;
                        q_t <= RX_DATA;
                    else
                        q_clk_count <= 0;
                        q_rx_data(q_bit_index) <= rx;
                        if q_bit_index < 7 then
                            q_bit_index <= q_bit_index + 1;
                            q_t <= RX_DATA;
                        else
                            q_bit_index <= 0;
                            q_t <= RX_STOP;
                        end if;
                    end if;

                when RX_STOP =>
                    if q_clk_count < CLK_COUNT-1 then
                        q_clk_count <= q_clk_count + 1;
                        q_t <= RX_STOP;
                    else
                        q_rx_done <= '1';
                        q_clk_count <= 0;
                        q_t <= COOLDOWN;
                    end if;

                when COOLDOWN =>
                    q_t <= IDLE;
                    q_rx_done <= '0';
                    
                when others =>
                    q_t <= IDLE;
            end case;
        end if;
    end process;

  rx_done <= q_rx_done;
  rx_dout <= q_rx_data;
  
end RTL;
