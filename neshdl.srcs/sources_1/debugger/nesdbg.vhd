library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity NesDbg is
    port (
        clk100: in std_logic;
        rst: in std_logic;
        addr_out: out unsigned(15 downto 0);
        dout: out unsigned(7 downto 0);
        active: out std_logic;
        cpu_rst: out std_logic;
        ines_header: out unsigned(39 downto 0);
        we: out std_logic;
        rx: in std_logic;   -- rx signal
        tx: out std_logic   -- tx signal
    );
end NesDbg;

architecture rtl of nesdbg is
    
    type state_t is (
        IDLE, ECHO, DECODE, COOLDOWN, 
        WRITE_MEM_READ, WRITE_MEM_WRITE,
        CPU_RESET, READ_NES_HEADER
        );
    
    constant CMD_ECHO:       unsigned(7 downto 0) := x"00";
    constant CMD_WRITE_MEM:  unsigned(7 downto 0) := x"01";
    constant CMD_CPU_RESET:  unsigned(7 downto 0) := x"02";
    constant CMD_NES_HEADER: unsigned(7 downto 0) := x"03";

    signal q_t, d_t: state_t := IDLE;
    signal q_data, d_data: unsigned(7 downto 0);
    signal q_cmd, d_cmd: unsigned(7 downto 0);
    signal q_nes_header, d_nes_header: unsigned(39 downto 0);

    -- UART signals
    signal uart_rx_done: std_logic;
    signal uart_rx_dout: unsigned(7 downto 0);
    signal uart_tx_active: std_logic;
    signal uart_tx_done: std_logic;
    signal uart_tx_send: std_logic;

    signal q_addr, d_addr: unsigned(15 downto 0);
    signal q_count, d_count: unsigned(15 downto 0);
    signal q_step, d_step: integer range 0 to 31;

begin

    uart_rx_c: entity work.uart_rx
        port map (
            clk100 => clk100,
            rx => rx,
            rx_done => uart_rx_done,
            rx_dout => uart_rx_dout
        );

    uart_tx_c: entity work.uart_tx
        port map (
            clk100 => clk100,
            tx_send => uart_tx_send,
            tx_din => q_data,
            tx_active => uart_tx_active,
            tx => tx,
            tx_done => uart_tx_done
        );

    process(clk100)
    begin
        if rising_edge(clk100) then
            if rst = '1' then
                q_t <= IDLE;
                q_data <= (others => '0');
                q_count <= (others => '0');
                q_addr <= (others => '0');
                q_step <= 0;
                q_cmd <= (others => '0');
            else
                q_t <= d_t;
                q_data <= d_data; 
                q_count <= d_count;
                q_addr <= d_addr;
                q_step <= d_step;
                q_cmd <= d_cmd;
                q_nes_header <= d_nes_header;
            end if;
        end if;
    end process;
    
    active <= '1' when q_t /= IDLE else '0';
    dout <= q_data;
    addr_out <= q_addr;
    ines_header <= q_nes_header;

    process(all)
    begin
        d_t <= q_t;
        d_data <= q_data;
        d_count <= q_count;
        d_addr <= q_addr;
        d_step <= q_step;
        d_cmd <= q_cmd;
        d_nes_header <= q_nes_header;

        cpu_rst <= '0';
        we <= '0';
        uart_tx_send <= '0';

        case q_t is
            when IDLE =>
                if uart_rx_done = '1' then
                    d_t <= DECODE;
                    d_cmd <= uart_rx_dout;
                    d_step <= 0;
                end if;
            
            when DECODE =>
                case q_cmd is
                    when CMD_ECHO =>
                        d_t <= ECHO;
                    when CMD_WRITE_MEM =>
                        d_t <= WRITE_MEM_READ;
                    when CMD_CPU_RESET =>
                        d_t <= CPU_RESET;
                    when CMD_NES_HEADER =>
                        d_t <= READ_NES_HEADER;
                    when others => 
                        d_t <= IDLE;
                end case;

            when ECHO =>
                if q_step = 0 then
                    if uart_rx_done = '1' then
                        d_data <= uart_rx_dout;
                        d_t <= ECHO;
                        d_step <= 1;
                    end if;
                elsif q_step = 1 then
                    uart_tx_send <= '1';
                    d_t <= COOLDOWN;
                end if;

            when CPU_RESET =>
                cpu_rst <= '1';
                d_t <= IDLE;

            when WRITE_MEM_READ =>
                if uart_rx_done = '1' then
                    d_step <= q_step + 1;
                    if q_step = 0 then 
                        d_addr(15 downto 8) <= uart_rx_dout;
                    elsif q_step = 1 then
                        d_addr(7 downto 0) <= uart_rx_dout;
                    elsif q_step = 2 then
                        d_count(15 downto 8) <= uart_rx_dout;
                    elsif q_step = 3 then
                        d_count(7 downto 0) <= uart_rx_dout;
                        d_step <= 0;
                        d_t <= WRITE_MEM_WRITE;
                    end if;
                end if;

            when WRITE_MEM_WRITE =>
                if q_step = 0 then
                    if uart_rx_done = '1' then
                        d_data <= uart_rx_dout;
                        d_step <= q_step + 1;
                    end if;
                elsif q_step = 1 then
                    we <= '1';
                    d_step <= q_step + 1;
                elsif q_step = 2 then
                    if q_count = 0 then
                        d_t <= IDLE;
                    else
                        d_count <= q_count - 1;
                        d_addr <= q_addr + 1;
                        d_step <= 0;
                    end if;
                end if;

            when READ_NES_HEADER =>
                if uart_rx_done = '1' then
                    d_step <= q_step + 1;
                    if q_step = 0 then
                        d_nes_header(39 downto 32) <= uart_rx_dout;
                    elsif q_step = 1 then
                        d_nes_header(31 downto 24)  <= uart_rx_dout;
                    elsif q_step = 2 then
                        d_nes_header(23 downto 16)  <= uart_rx_dout;
                    elsif q_step = 3 then
                        d_nes_header(15 downto 8)  <= uart_rx_dout;
                    elsif q_step = 4 then
                        d_nes_header(7 downto 0) <= uart_rx_dout;
                        d_t <= IDLE;
                    end if;
                end if; 

            when COOLDOWN =>
                if uart_tx_done = '1' then
                    d_t <= IDLE;
                    d_data <= (others => '0');
                end if;

            when others =>
                d_t <= IDLE;
        end case;
    end process;

    
end architecture rtl;