library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;
use STD.textio.all;

library WORK;

entity nes_sim is
end nes_sim;

architecture Behavioral of nes_sim is
-- Cpu signals
signal cpu_address: unsigned(15 downto 0);
signal cpu_data_out: unsigned(7 downto 0);
signal cpu_data_in: unsigned(7 downto 0);
signal cpu_irq_n: std_logic := '1';
signal cpu_nmi_n: std_logic := '1';
signal cpu_res_n: std_logic := '1';
signal cpu_sync: std_logic;
signal cpu_we_n: std_logic;

signal std_data_out: std_logic_vector(7 downto 0);
signal std_address_out: std_logic_vector(15 downto 0);

signal clk: std_logic;
signal rst: std_logic;
signal ce: std_logic;

-- simulator
signal sim_a: unsigned(7 downto 0);
signal sim_x: unsigned(7 downto 0);
signal sim_y: unsigned(7 downto 0);
signal sim_status: unsigned(7 downto 0);
signal sim_sp: unsigned(7 downto 0);
signal sim_pc: unsigned(15 downto 0);
signal sim_count: unsigned(31 downto 0);
signal sim_write: std_logic;


begin
    
    process
    begin
        clk <= '0';
        wait for 5ns;
        clk <= '1';
        wait for 5ns;
    end process;

    process
    begin
        ce <= '1';
        wait;
    end process;

    process
    begin
        wait for 10ns;
        rst <= '1';
        wait for 10ns;
        rst <= '0';
        wait;
    end process;

    process(sim_write)
        file output_file: text open write_mode is "6502_sim.txt";
        variable row: line;
    begin
        if (sim_write) = '1' then
            write(
                row,
                integer'image(to_integer(sim_pc)) & "    " &
                "A:" & integer'image(to_integer(sim_a)) & " " &
                "X:" & integer'image(to_integer(sim_x)) & " " &
                "Y:" & integer'image(to_integer(sim_y)) & " " &
                "P:" & integer'image(to_integer(sim_status)) & " " &
                "SP:" & integer'image(to_integer(sim_sp)) & "  " &
                "CYC:" & integer'image(to_integer(sim_count) - 1) & " " &
                "AD:" & integer'image(to_integer(cpu_address)) & " " &
                "DI:" & integer'image(to_integer(cpu_data_in)) & " " &
                "DO:" & integer'image(to_integer(cpu_data_out))
            );
            writeline(output_file, row);
        end if;
    end process;

    cpu: entity work.cpu_r2a03
        port map (
            clk1_8 => clk,
            rst => rst,
            nmi_n => '1',
            irq_n => '1',
            addr => cpu_address,
            din => cpu_data_in,
            dout => cpu_data_out,
            ce => ce,
            we_n => cpu_we_n,
            sim_a => sim_a,
            sim_x => sim_x,
            sim_y => sim_y,
            sim_pc => sim_pc,
            sim_sp => sim_sp,
            sim_status => sim_status,
            sim_count => sim_count,
            sim_write => sim_write
        );
    
    ram: entity work.Ram
        port map (
            clk => clk,
            address => cpu_address,
            we_n => cpu_we_n,
            data_in => cpu_data_out,
            data_out => cpu_data_in
        );

end Behavioral;
