library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity mem_test is
end mem_test;

architecture Behavioral of mem_test is
    signal clk: std_logic;
    signal addr: unsigned(7 downto 0);
    signal din: unsigned(7 downto 0);
    signal dout: unsigned(7 downto 0);
    signal we: std_logic;
begin

    mem_c: entity work.single_port_ram
        generic map (
            address_width => 8,
            data_width => 8
        )
        port map (
            clk100 => clk,
            ce => '1',
            we => we,
            addr => addr,
            din => din,
            dout => dout
        );

    process
    begin
        clk <= '0';
        wait for 5ns;
        clk <= '1';
        wait for 5ns;
    end process;
    
    process
    begin
        wait for 10ns;
        addr <= x"25";
        din <= x"20";
        we <= '1';
        wait for 10ns;
        we <= '0';
        assert dout /= x"20";
        addr <= x"30";
        wait for 10ns;
        addr <= x"40";
        wait;
    end process;

end Behavioral;
