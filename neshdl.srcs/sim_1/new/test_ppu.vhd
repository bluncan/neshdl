library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity test_ppu is
end test_ppu;

architecture Behavioral of test_ppu is
    signal clk100: std_logic;
    signal clk_cpu, clk_ppu, clk_vga: std_logic;
    signal rst: std_logic;

    signal cpu_ce: std_logic;
    signal cpu_nmi_n: std_logic;
    signal cpu_irq_n: std_logic;
    signal cpu_din: unsigned(7 downto 0);
    signal cpu_dout: unsigned(7 downto 0);
    signal cpu_addr: unsigned(15 downto 0);
    signal cpu_we_n: std_logic;
    -- Simulation
    signal cpu_sim_a: unsigned(7 downto 0);
    signal cpu_sim_x: unsigned(7 downto 0);
    signal cpu_sim_y: unsigned(7 downto 0);
    signal cpu_sim_sp: unsigned(7 downto 0);
    signal cpu_sim_status: unsigned(7 downto 0);
    signal cpu_sim_pc: unsigned(15 downto 0);
    signal cpu_sim_count: unsigned(31 downto 0);
    signal cpu_sim_write: std_logic;

    -- PPU
    signal ppu_bus_we: std_logic;
    signal ppu_bus_addr: unsigned(13 downto 0);
    signal ppu_bus_dout: unsigned(7 downto 0);
    signal ppu_bus_din: unsigned(7 downto 0);
    signal ppu_din: unsigned(7 downto 0);
    signal ppu_dout: unsigned(7 downto 0);
    signal ppu_rw_reg: std_logic;
    
    -- Work ram
    signal wram_ce: std_logic;
    signal wram_dout: unsigned(7 downto 0);

    -- Vram
    signal vram_ce: std_logic;
    signal vram_dout: unsigned(7 downto 0);
    signal vram_addr: unsigned(10 downto 0);

    -- PRGROM memory
    signal prgrom_ce: std_logic;
    signal prgrom_dout: unsigned(7 downto 0);

    -- CHR Pattern Table memory
    signal chram_ce: std_logic;
    signal chram_dout: unsigned(7 downto 0);

    signal ppu_ri_ncs: std_logic;
    signal ppu_ri_dout: unsigned(7 downto 0);

    -- VGA signals
    signal hsync, vsync: std_logic;
    signal red, green, blue: unsigned(3 downto 0);
begin

    process
    begin
        clk100 <= '0';
        wait for 5ns;
        clk100 <= '1';
        wait for 5ns;
    end process;

    process
    begin
        rst <= '1';
        wait for 1000ns;
        rst <= '0';
        wait;
    end process;

    clk_gen_c: entity work.clock_gen
        port map (
            clk100 => clk100,
            rst => rst,
            clk_cpu => clk_cpu,
            clk_ppu => clk_ppu,
            clk_vga => clk_vga
        );

    cpu_ce <= '1';
    -- R2A03 CPU
    cpu_c: entity work.cpu_r2a03 
        port map (
            clk1_8 => clk_cpu,
            rst => rst,
            ce => cpu_ce,
            nmi_n => cpu_nmi_n,
            irq_n => '1',
            din => cpu_din,
            dout => cpu_dout,
            addr => cpu_addr,
            we_n => cpu_we_n,
            -- Simulations
            sim_a => cpu_sim_a,
            sim_x => cpu_sim_x,
            sim_y => cpu_sim_y,
            sim_sp => cpu_sim_sp,
            sim_status => cpu_sim_status,
            sim_pc => cpu_sim_pc,
            sim_count => cpu_sim_count,
            sim_write => cpu_sim_write
        );

    -- 2C02 PPU
    ppu_rw_reg <= '0' when cpu_addr(15 downto 13) = "001" else '1';
    ppu_c: entity work.ppu_2c02
        port map (
            clk5_1 => clk100,
            clk100 => clk100,
            clk25 => clk100,
            rst => rst,
            din => cpu_dout,
            dout => ppu_dout,
            addr => cpu_addr(2 downto 0),
            we_n => cpu_we_n,
            ce_n => ppu_rw_reg,
            vbl_n => cpu_nmi_n,
            bus_addr => ppu_bus_addr,
            bus_we => ppu_bus_we,
            bus_din => ppu_din,
            bus_dout => ppu_bus_dout,
            hsync => hsync,
            vsync => vsync,
            red => red,
            green => green,
            blue => blue
        );
    
    -- Work ram $0000 - $1FFF
    -- 2KB ram, mirrored to 1FFFF
    wram_ce <= (not cpu_addr(15)) and (not cpu_addr(14)) and (not cpu_addr(13));
    wram_c: entity work.single_port_ram
        generic map (
            address_width => 11,
            data_width => 8
        )
        port map (
            clk100 => clk100,
            ce => wram_ce,
            we => not cpu_we_n,
            addr => cpu_addr(10 downto 0),
            din => cpu_dout,
            dout => wram_dout
        );

    -- Vram 0x2000 - 0x3FFF (Nametable memory)
    vram_ce <= ppu_bus_addr(13);
    --vram_ce <= '1' when ppu_bus_addr >= x"2000" and ppu_bus_addr <= x"3FFF" else '0';
    vram_c: entity work.single_port_ram
        generic map (
            address_width => 11,
            data_width => 8
        )
        port map (
            clk100 => clk100,
            ce => vram_ce,
            we => ppu_bus_we,
            addr => ppu_bus_addr(11) & ppu_bus_addr(9 downto 0), 
            din => ppu_bus_dout,
            dout => vram_dout 
        );

    -- PRGROM memory 0x8000 - 0xFFFF
    prgrom_ce <= cpu_addr(15);
    prgrom_c: entity work.prgrom_nestest
            port map (
                clk_in => clk100,
                ce => prgrom_ce,
                we => not cpu_we_n,
                addr_a => '0' & cpu_addr(13 downto 0),
                din_a => cpu_dout,
                dout_a => prgrom_dout
            );
    
    -- CHRAM memory 0x0000-0x1FFF
    chram_ce <= not ppu_bus_addr(13);
  chram_c: entity work.chrom_nestest
            port map(
                clk_in => clk100,
                we => ppu_bus_we,
                ce => chram_ce,
                addr_a => ppu_bus_addr(12 downto 0),
                din_a => ppu_bus_dout,
                dout_a => chram_dout
            );

    ppu_din <= chram_dout when chram_ce = '1' else 
               vram_dout;
    
    
    cpu_din <= ppu_dout when ppu_rw_reg = '1' else
               prgrom_dout when prgrom_ce = '1' else 
               wram_dout when wram_ce = '1' else 
               ppu_dout;
    --cpu_din <= prgrom_dout or wram_dout or ppu_ri_dout;

end Behavioral;
