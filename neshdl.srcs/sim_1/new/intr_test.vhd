library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity intr_test is
end intr_test;

architecture Behavioral of intr_test is
    signal clk: std_logic;
    signal rst: std_logic;
    signal nmi_n: std_logic;

    signal cpu_address: unsigned(15 downto 0);
    signal cpu_data_out: unsigned(7 downto 0);
    signal cpu_data_in: unsigned(7 downto 0);
    signal cpu_we_n: std_logic;
    signal cpu_ce: std_logic := '1';
begin

    process
    begin
        clk <= '0';
        wait for 5ns;
        clk <= '1';
        wait for 5ns;
    end process;

    process
    begin
        rst <= '1';
        wait for 10ns;
        rst <= '0';
        wait for 10ns;
        
        nmi_n <= '1';
        wait for 100ns;
        nmi_n <= '0';
        wait for 10ns;
        nmi_n <= '1';
    end process;


    cpu: entity work.cpu_r2a03
    port map (
        clk1_8 => clk,
        rst => rst,
        nmi_n => nmi_n,
        irq_n => '1',
        addr => cpu_address,
        din => cpu_data_in,
        dout => cpu_data_out,
        ce => cpu_ce,
        we_n => cpu_we_n
    );


prgrom: entity work.prgrom_nestest
    port map (
        clk_in => clk,
        ce => cpu_ce,
        addr_a => cpu_address(14 downto 0),
        we => not cpu_we_n,
        din_a => cpu_data_out,
        dout_a => cpu_data_in
    );

end Behavioral;
