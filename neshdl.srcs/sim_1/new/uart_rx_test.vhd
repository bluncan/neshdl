library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

entity uart_rx_test is
end uart_rx_test;

architecture Behavioral of uart_rx_test is
    signal clk: std_logic;
    signal rx: std_logic;
    signal rx_done: std_logic;
    signal rx_dout: unsigned(7 downto 0);
begin

    uart_tx_c: entity work.uart_rx
        generic map (
            CLK_COUNT => 4
        )
        port map (
            clk100 => clk,
            rx => rx,
            rx_done => rx_done,
            rx_dout => rx_dout
        );

    process
    begin
        clk <= '0';
        wait for 5 ns;
        clk <= '1';
        wait for 5 ns;
    end process;

    process
    begin
        rx <= '0';
        wait for 4 * 10 ns;

        rx <= '1';
        wait for 4 * 10 ns;
        rx <= '0';
        wait for 4 * 10 ns;
        rx <= '1';
        wait for 4 * 10 ns;
        rx <= '0';
        wait for 4 * 10 ns;
        rx <= '1';
        wait for 4 * 10 ns;
        rx <= '0';
        wait for 4 * 10 ns;
        rx <= '1';
        wait for 4 * 10 ns;
        rx <= '0';
        wait for 4 * 10 ns;
        assert rx_dout = "01010101";
        rx <= '1';
        wait;
    end process;


end Behavioral;
